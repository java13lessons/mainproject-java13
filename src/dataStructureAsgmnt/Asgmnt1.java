package dataStructureAsgmnt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Asgmnt1 {

	public static void main(String[] args) {
		/// a.
		List<String> strings = new ArrayList<String>();
		strings.add("Computer");
		strings.add("Plate");
		strings.add("Chair");
		strings.add("Girl");
		strings.add("Boy");
		strings.add("Cat");
		strings.add("Dog");
		strings.add("Shirt");
		strings.add("Determination");

		/// We will use the same iterator for all the steps, where loop is needed
		// b,c,d,e,f
		int countStartC = 0;
		int countEndE = 0;
		int countLen5 = 0;
		int countCharE = 0;
		int countTe = 0;

		Iterator<String> iterator = strings.iterator();
		while (iterator.hasNext()) {
			String currentElement = iterator.next();
			if (currentElement.startsWith("C"))
				countStartC++;
			if (currentElement.endsWith("e")) //// can't use else if, because there can be the string which starts with
												//// C and ends with e
				countEndE++;
			if (currentElement.length() == 5)
				countLen5++;

			if (currentElement.contains("e"))
				countCharE++;

			if (currentElement.contains("te"))
				countTe++;
		}
		if (countStartC > 1)
			System.out.println("There are " + countStartC + " elements, starting with C");
		else if (countStartC == 1)
			System.out.println("There are " + countStartC + " element, starting with C");
		else
			System.out.println("There are no elements, starting with C");

		System.out.println("There are " + countEndE + " elements, ending with e");
		System.out.println("There are " + countLen5 + " elements, with the lenght 5");
		System.out.println("There are " + countCharE + " elements, which contain character e");
		System.out.println("There are " + countTe + " elements, which contain te");
		System.out.print(System.lineSeparator());

		// g. We will use Map, to implement it
		Map<Integer, Integer> hist = new TreeMap<Integer, Integer>(); /// key - length, value - number of the element
		iterator = strings.iterator();
		while (iterator.hasNext()) {
			String str = iterator.next();
			int len = str.length();
			// If the entry with the key len already exists in the table
			if (hist.containsKey(len)) {
				int numberOfTheElements = hist.get(len) + 1; /// another string with the size len is met
				hist.put(len, numberOfTheElements);
			} else
				hist.put(len, 1); /// since this is the first, the string with the length len is met, then number
									/// of the elements with the length len is 1 at this moment
		}

		Iterator<Integer> iteratorKeys = hist.keySet().iterator(); /// we will iterate through all the keys of the hist
																	/// Map
		while (iteratorKeys.hasNext()) {
			int len = iteratorKeys.next();
			System.out.println(len + " -> " + hist.get(len));
		}
//Sort the list
		System.out.print(System.lineSeparator());
		printList(sort(strings));

	}

	/// Sorting
	/// using bubble sort
	private static List<String> sort(List<String> list) {
		List<String> sortedList = new ArrayList<String>(list);/// this is the way how we can copy two collection (need
																/// to pass the list, which needs to be copied to the
																/// constructor)

		// For the sorting it is easier to use the loop, not iterator
		/// In addition, it is hard to use the iterators, if we want to mutate the list
		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (sortedList.get(i).compareTo(sortedList.get(j)) > 0) {
					String swap = sortedList.get(i);
					sortedList.set(i, sortedList.get(j));
					sortedList.set(j, swap);
				}

			}
		}

		return sortedList;

	}

	private static void printList(List<String> list) {
		Iterator<String> iterator = list.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}
}
