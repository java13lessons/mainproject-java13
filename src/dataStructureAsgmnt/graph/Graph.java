package dataStructureAsgmnt.graph;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class Graph {

//	private Set<Vertex> vertexes = new TreeSet<Vertex>();/// the set where all the vertexes of the graph are stored.
//															/// Only vertexes with unique names are allowed

	private Map<String, Vertex> vertexes = new TreeMap<String, Vertex>();/// we will use Map, to make it easier to find
																			/// the vertex by name. Key - name, Value -
																			/// Vertex
	private Set<Edge> edges = new HashSet<Edge>();/// edges stored here

	public static void main(String[] args) throws IOException {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Input file path:");
		Graph graph = new Graph(sc.nextLine());
		graph.process();
		sc.close();
	}

	/// this will be the main method for the object
	public void process() {
		printElements(new HashSet<Object>(this.edges));/// print the elements of the graph
		System.out.println("There are " + this.edges.size() + " direct flights");
		System.out.println("Airports connected to Cape Town:");
		printElements(new HashSet<Object>(this.vertexes.get("Cape Town").getEdges()));
		System.out.println("There are " + this.vertexes.get("Jo'burg").getEdges().size() + " flights from Jo'burg");

	}

	public Graph(String filePath) throws IOException {
		File fileCSV = new File(filePath);
		BufferedReader reader = new BufferedReader(new FileReader(fileCSV, StandardCharsets.UTF_8));
///In this case - we know the structure of the file, 1st line - names of the Vertexes, 2nd line - the edges
		// Since we know there are two lines - no need to iterate through the lines
		String line1 = reader.readLine();
		String[] vertexesNames = line1.split(","); /// split the line and we get the array of the names of the vertexes
		for (String vertexName : vertexesNames)
			this.vertexes.put(vertexName, new Vertex(vertexName));

		String line2 = reader.readLine();
		String[] edges = line2.split(",");
		for (String edgeContent : edges) {
			/// the structure of the edges in the file is vertex1-vertex2
			String[] vertexesInEdge = edgeContent.split("-"); /// here we have the array two vertexes
			String name1 = vertexesInEdge[0];
			String name2 = vertexesInEdge[1];
			Vertex vertex1 = this.vertexes.get(name1);
			Vertex vertex2 = this.vertexes.get(name2);

			Edge edge = new Edge(vertex1, vertex2);
			this.edges.add(edge);
			vertex1.getEdges().add(edge);
			vertex2.getEdges().add(edge);
		}

		reader.close();

	}

//	public Graph() {/// we will populate the graph in the constructor
//		java.util.Scanner sc = new java.util.Scanner(System.in);
//		System.out.println("Enter the number of vertexes: ");
//		int numberOfVertexes = Integer.parseInt(sc.nextLine());
//
//		/// Create the vertexes
//		for (int i = 0; i < numberOfVertexes; i++) {
//			System.out.println("Enter the name for the vertex " + i);
//			String name = sc.nextLine();/// read the name of the vertex
//			this.vertexes.put(name, new Vertex(name));
//		}
//
//		/// The edge
//		System.out.println("Enter the number of edges: ");
//		int numberOfEdges = Integer.parseInt(sc.nextLine());
//		for (int i = 0; i < numberOfEdges; i++) {
//			System.out.println("Edge:");
//			System.out.println("Enter the name of the vertex");
//			String name1 = sc.nextLine();/// read the name of the vertex
//			System.out.println("Enter the name of the vertex");
//			String name2 = sc.nextLine();/// read the name of the vertex
//			Vertex vertex1 = this.vertexes.get(name1);
//			Vertex vertex2 = this.vertexes.get(name2);
//
//			Edge edge = new Edge(vertex1, vertex2);
//			this.edges.add(edge);
//			vertex1.getEdges().add(edge);
//			vertex2.getEdges().add(edge);
//
//		}
//		sc.close();
//	}

	/// If we want to work with any type of the element, we can use Object type
	public static void printElements(Collection<Object> elements) {
		Iterator<Object> iterator = elements.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());
	}

}
