package dataStructureAsgmnt.graph;

import java.util.HashSet;
import java.util.Set;

public class Vertex implements Comparable<Vertex> { // to ensure, that the Vertexes are unique, we will use TreeSet, so
													// need to implement Comparable interface

//	private int index;
	private String name;
	private Set<Edge> edges = new HashSet<Edge>();//// the set of the edges, where this vertex occurs

	public Set<Edge> getEdges() {
		return edges;
	}

	public Vertex(String name) {
		this.name = name;
	}

	@Override
	public int compareTo(Vertex o) {
		return this.name.compareTo(o.name);
	}

	@Override
	public String toString() {
		return this.name;
	}

}
