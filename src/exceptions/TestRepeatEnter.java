package exceptions;

public class TestRepeatEnter {

	public static void main(String[] args) {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Please, enter the values:");

		/// Divide value1/value2
		int value1 = 0, value2 = 0, result = 0;
		boolean resultCalculated = false;

		while (!resultCalculated) {
///If the value1 is entered
			boolean value1IsEntered = false;
			while (!value1IsEntered) {
				try {
					System.out.println("Enter value1:");
					value1 = Integer.parseInt(sc.nextLine());
					value1IsEntered = true;
				} catch (NumberFormatException e) {
					System.out.println("Value is entered incorrectly, please, repeat!");
				}
			}

			boolean value2IsEntered = false;
			while (!value2IsEntered) {
				try {
					System.out.println("Enter value2:");
					value2 = Integer.parseInt(sc.nextLine());
					value2IsEntered = true;
				} catch (NumberFormatException e) {
					System.out.println("Value is entered incorrectly, please, repeat!");
				}
			}

			try {
				result = value1 / value2;
				resultCalculated = true;
				System.out.println("The result is " + result);
			} catch (ArithmeticException e) {
				System.out.println("The values trigger the issue in the calculation, please, enter different values");
			}
		}
		sc.close();
	}
}
