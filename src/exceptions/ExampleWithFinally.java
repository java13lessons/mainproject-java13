package exceptions;

public class ExampleWithFinally {

	public static void main(String[] args) {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		try {

			int value1 = Integer.parseInt(sc.nextLine());
			int value2 = Integer.parseInt(sc.nextLine());
			int result = value1 / value2;
			return;

		} catch (NumberFormatException e) {
			System.out.println("NumberFormatException is occured");
		}

		catch (ArithmeticException e) {
			System.out.println("Arritmetic exception is occured");
		} finally {
			System.out.println("Finally block is trigerred");
		}
		System.out.println("The code is outside try,catch");
		sc.close();
	}

}
