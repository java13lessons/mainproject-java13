package exceptions;

public class Examples {

	public static void main(String[] args) {

		/// Try to access the variable, which has no reference (is null)
		String var = null;
		try {
//			var.charAt(2);

			// Attempt to access the element of the array, which is greater than the places
			// in the array
			int[] arr = new int[2];
			arr[2] = 4;

			// Class cast exception - try to cast the superclass to the subclass
//			SuperClass obj = new SuperClass();
//			SubClass objSub = (SubClass) obj;
		}
//		catch (NullPointerException e) {
//			System.out.println("Null Pointer Exception occured");
//			e.printStackTrace();
//		} catch (ArrayIndexOutOfBoundsException e) {
//			System.out.println("Bounds out of array exception is occured");
//		}
		catch (NullPointerException | ArrayIndexOutOfBoundsException e) {
			System.out.println("NullPointerException or ArrayIndexOutOfBoundsException is orrucerd");
		}
		System.out.println("Code after the exception is occured");

	}

}

class SuperClass {
}

class SubClass extends SuperClass {
}
