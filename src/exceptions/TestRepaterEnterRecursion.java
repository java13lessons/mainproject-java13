package exceptions;

public class TestRepaterEnterRecursion {

	private static java.util.Scanner sc = new java.util.Scanner(System.in);

	public static void main(String[] args) {
		divide();
	}

	private static void divide() {
		int value1 = inputValue();
		int value2 = inputValue();

		try {
			int result = value1 / value2;
			System.out.println("The result is " + result);
		} catch (Exception e) {
			System.out.println("The values trigger the issue in the calculation, please, enter different values");
			divide();
		}
	}

	private static int inputValue() {
		System.out.println("Enter the value:");
		try {
			return Integer.parseInt(sc.nextLine());
		} catch (Exception e) {
			System.out.println("The value entered is not correct, please, try again");
			return inputValue();
		}
	}

}
