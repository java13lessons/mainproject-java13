package exceptions;

public class InputExceptions {

	public static void main(String[] args) {

		java.util.Scanner sc = new java.util.Scanner(System.in);
		/// Division by zero, text, instead of the integer value
//		int value1;
//		int value2;
//
//		try {
//			System.out.println("Enter value 1:");
//			value1 = Integer.parseInt(sc.nextLine());
//			System.out.println("Enter value 2:");
//			value2 = Integer.parseInt(sc.nextLine());
//
//			try {
//				if (value2 == 0)
//					throw new DivisionByZero(value1, value2);
//
//				int result = value1 / value2;
//				System.out.println("The result is " + result);
//			} catch (ArithmeticException e) {//// is unreachable right now
//				System.out.println("Cannot divide by zero");
//			} catch (DivisionByZero e) {
//				e.printError();
//			}
//
//		} catch (NumberFormatException e) {
//			System.out.println("You can enter integer number only");
//		}

		// Get character of the string
		System.out.println("Enter the string:");
		String value = sc.nextLine();
		System.out.println("Enter the position of the character:");
		int position = -1;////dummy entry, to get rid of the compiler error
		try {
			position = Integer.parseInt(sc.nextLine()) - 1;
			char character = value.charAt(position);
			System.out.println("The character at the position " + (position + 1) + " is " + character);
		} catch (StringIndexOutOfBoundsException e) {
			System.out.println("The position " + (position + 1) + " is incorrect");
		} catch (NumberFormatException e) {
			System.out.println("You can not enter the letters as the position");
		}

		sc.close();
	}

}
