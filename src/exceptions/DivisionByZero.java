package exceptions;

public class DivisionByZero extends Exception {

	private int value1, value2;

	/**
	 * 
	 */
	private static final long serialVersionUID = -3576268961220249590L;

	public int getValue1() {
		return value1;
	}

	public int getValue2() {
		return value2;
	}

	public DivisionByZero(int value1, int value2) {
		this.value1 = value1;
		this.value2 = value2;
	}

	public void printError() {
		System.err.println("You've tried to divide " + this.value1 + " by " + this.value2);
	}

}
