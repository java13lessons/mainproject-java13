package collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import inheritance.Employee;

public class TestMaps {

	public static void main(String[] args) {
		Map<Integer, Employee> employees = new TreeMap<Integer, Employee>();
		Employee employee1 = new Employee("Sara", "Connor", 24, 21);
		Employee employee2 = new Employee("James", "Cramer", 51, 32);
		Employee employee3 = new Employee("Nile", "Zuniga", 31, 10);
		employees.put(8, employee1);
		employees.put(10, employee2);
		employees.put(3, employee3);
//		printMap(employees);

		Map<Integer, Employee> employeesHash = new HashMap<Integer, Employee>();
		employeesHash.put(312, employee1);
		employeesHash.put(323, employee2);
		employeesHash.put(783, employee3);
		printMap(employeesHash);
		//// We can see that the sequence of the elements stored is hashed
	}

	public static void printMap(Map<Integer, Employee> collection) {
		Iterator<Employee> iterator = collection.values().iterator(); /// for Map structure we need to call the method
																		/// values()
		while (iterator.hasNext()) {
			Employee employee = iterator.next();
			System.out.println("Name, Surname : " + employee.getName() + " " + employee.getSurname());

		}
	}

}
