package collections;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import oop.Officer;

public class TestSets {

	public static void main(String[] args) {

		/// We can only use the Set (ex. TreeSet) for the Object types (Classes), which
		/// implement Comparable interface. Otherwise, the exception will occur, after
		/// the program is executed
//		Set<Officer> set = new Set<Officer>();
		Set<Officer> set = new HashSet<Officer>(); /// for the HashSet compareTo method is not used (uniqueness is
													/// defined by reference)
		/// 1.All the values must be unique in Set
		/// 2. The uniqueness is checked using compareTo method
		Officer officer1 = new Officer("Jack", "Johnson", "District1", 23, 10);
		Officer officer2 = new Officer("Jack", "Johnson", "District2", 10, 30);
		set.add(officer1);
		set.add(officer2);

		/// Here we can see, that the Set is sorted, using the way we defined in
		/// oop.Officer class
		Iterator<Officer> iterator = set.iterator();
		while (iterator.hasNext())
			System.out.println(iterator.next());

	}

}
