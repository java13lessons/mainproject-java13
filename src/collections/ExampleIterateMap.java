package collections;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

public class ExampleIterateMap {

	public static void main(String[] args) {
		Map<Integer, String> map = new TreeMap<Integer, String>();
		map.put(1, "Element1");
		map.put(2, "Element2");
		map.put(7, "Element0");
		map.put(4, "Element4");
		map.put(0, "Element1");
		map.put(5, "Element9");
		map.put(6, "Element19");
		map.put(3, "Element17");
		map.put(-4, "Element13");
		map.put(8, "Element13");
		map.put(10, "Element13OP");
		map.put(17, "Element12fd");
		map.put(92, "Element131");
		map.put(72, "Element1312");

		Iterator<Entry<Integer, String>> iterator = map.entrySet().iterator();
		while (iterator.hasNext()) {
			Entry<Integer, String> entry = iterator.next();
			System.out.println("Key = " + entry.getKey() + "; Value = " + entry.getValue());
		}

		Iterator<Integer> iterator2 = map.keySet().iterator();
		while (iterator2.hasNext())
			System.out.println(iterator2.next());

		Iterator<String> iterator3 = map.values().iterator();
		while (iterator3.hasNext())
			System.out.println(iterator3.next());

	}

}
