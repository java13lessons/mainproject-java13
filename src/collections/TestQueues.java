package collections;

import java.util.ArrayDeque;

public class TestQueues {

	public static void main(String[] args) {
		///For ArrayDeque - the concept is First In First Out
		ArrayDeque<Integer> col = new ArrayDeque<Integer>();
		col.add(4343);
		col.add(6132);
		col.add(2132);
		col.add(4533);
		System.out.println(col.peek());
		col.remove();
		System.out.println(col.peek());
		
	}

}
