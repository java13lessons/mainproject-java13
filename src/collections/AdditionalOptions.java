package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class AdditionalOptions {

	public static void main(String[] args) {

		List<Integer> integers = new ArrayList<Integer>();
		integers.add(34);
		integers.add(10);
		integers.add(90);
		integers.add(13);
		integers.add(7);
		Collections.shuffle(integers);
//		printList(integers);

		System.out.println("Max value:" + Collections.max(integers));
		System.out.println("Min value:" + Collections.min(integers));

	}

	private static void printList(List<Integer> list) {
		Iterator<Integer> iterator = list.iterator();

		while (iterator.hasNext()) {
			System.out.println(iterator.next());

		}
	}
}
