package additional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class Split {

	public static void main(String[] args) {
		List<String> participants = new ArrayList<String>();

		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the name:");
		String name = sc.nextLine();
		while (!name.equals("")) {
			participants.add(name);
			name = sc.nextLine();
		}
		Collections.shuffle(participants);

		Iterator<String> iterator = participants.iterator();
		int members = 0;

		while (iterator.hasNext()) {
			if (members == 2) {
				System.out.print(System.lineSeparator());
				members = 0;
			}
			members++;
			System.out.print(iterator.next());
			System.out.print(" ");
		}

		sc.close();

	}

}
