package loops;

public class ForLoops {

	public static void main(String[] args) {

		for (int i = 0; i < 10; i++) { /// 1st component
			///// - declare and initilize the variable, to be used in this loop
			//// 2nd component - condition true/false. If it is empty - true by default
			///// 3rd component - operation/action, that is done after each loop
			///// 1st and 2nd components are executed/checked before the first iteration
			///// 3rd component - executed/checked after each iteration
			////i++ the same as i = i + 1;

			System.out.println("Inside the loop");
		}

	}

}
