package loops;

public class Cinema {

	public static void main(String[] args) {
		//// We are in the cinema, where there are n x m seats (n-rows, m-columns)
		java.util.Scanner sc = new java.util.Scanner(System.in);

		System.out.println("How many rows?");
		int n = Integer.parseInt(sc.nextLine());

		System.out.println("How many columns?");
		int m = Integer.parseInt(sc.nextLine());

		boolean seats[][] = new boolean[n][m];///// points if the seats are occupied
		///// by default - boolean is false ==>>> all the elements of the arrays will be
		///// false (at the start)

		while (seatsAvailable(seats)) {
			printSeats(seats);

			System.out.println("Would you like to buy or to return the ticket? (type b - to buy, type r - to return");
			char option = sc.nextLine().charAt(0);

			if (option == 'b') {
				System.out.println("Which row you would like to seat at?");
				int rowEntered = Integer.parseInt(sc.nextLine()) - 1;/// - 1, since we start at 0
				System.out.println("Which column you would like to seat at?"); /// - 1, since we start at 0
				int columnEntered = Integer.parseInt(sc.nextLine()) - 1;

				if (seats[rowEntered][columnEntered] == false) /// if it is not occupied
					seats[rowEntered][columnEntered] = true; /// occupy it
				else // otherwise -- it means means that the row is already occupied
					System.out.println("Sorry, the seat you selected is not available");
			} else if (option == 'r') {
				System.out.println("What is the row at your ticket?");
				int rowEntered = Integer.parseInt(sc.nextLine()) - 1;/// - 1, since we start at 0
				System.out.println("What is the column at your ticket?"); /// - 1, since we start at 0
				int columnEntered = Integer.parseInt(sc.nextLine()) - 1;

				if (seats[rowEntered][columnEntered] == true) /// if it is occupied
					seats[rowEntered][columnEntered] = false; /// make it avalaible
				else // otherwise -- it means means that the row is already occupied
					System.out.println("You are liar, this seat has not been occupied!");
			}
		}

		System.out.println("SOLD OUT!");
		sc.close();
	}

	private static boolean seatsAvailable(boolean seats[][]) {

		for (int i = 0; i < seats.length; i++) {/// go through all the rows
			for (int j = 0; j < seats[i].length; j++) { /// go through all the columns
				if (seats[i][j] == false)/// if there exists empty seat - return true, which means there it true, that
											/// the empty seat is available
					return true;
			}
		}

		return false;
	}

	private static void printSeats(boolean seats[][]) {
		for (int i = 0; i < seats.length; i++) {

			for (int j = 0; j < seats[i].length; j++) {
				if (seats[i][j] == true)
					System.out.print("X");
				else
					System.out.print("O");
				System.out.print(" ");
			}
			System.out.print(System.lineSeparator());
		}
	}

}
