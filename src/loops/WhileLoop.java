package loops;

import java.util.Scanner;

public class WhileLoop {

	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);// will monitor the console input
		String answer = "";

		do {///will be executed at least once
			// repeat action
			System.out.println("Hello! Do You want to repeat?");
			answer = sc.next();
		} while (answer.equals("Yes"));

//		int randomValue;
//		int numberOfItertations = 0;
//
//		do {///will be executed at least once
//			randomValue = (int) (Math.random() * 5) + 1;//// generate random value from 1 to 5
//			numberOfItertations++;
//		} while (randomValue != 3);
//		System.out.println(numberOfItertations);

//		Scanner sc = new Scanner(System.in);// will monitor the console input
//		String answer = sc.next();// read the user answer from console
//		while (answer.equals("Yes"))// if user answer is Yes, than repeat loop
//		{
//			// repeat action
//			System.out.println("Hello! Do You want to repeat?");
//			answer = sc.next();
//		}

		//// Endless loop
//		while (true) {
//			/// any logic inside
//			System.out.println("Endless loop");
//		}

//		int randomValue = 0;
//		int numberOfItertations = 0;
//
////		while (!(randomValue == 3)) {
//		while (randomValue != 3) {///equivalent aproach
//			randomValue = (int) (Math.random() * 5) + 1;//// generate random value from 1 to 5
//			numberOfItertations++;
//		}
//
//		System.out.println(numberOfItertations);

//		boolean flag = true;
//		int[] arr = new int[5];
//		int intval = 0;

//		//// while intval is less than 5, repeat action arr[intval] = intval++;
//		while (intval < 5)
//			arr[intval] = intval++;

//		////go inside if flag is true
//		while (flag == true) {
//			
//			////assign value intval to the intval (indexed) element of the array arr
//			arr[intval] = intval++;///aftet the value is assigned, incremement intval
//			
//			if (intval == 5)////if intval is 5, change flag, and while will not be executed 
//				/////at the next iteration
//				flag = false;
//
//		}

//		for (int element : arr)
//			System.out.println(element);

	}

}
