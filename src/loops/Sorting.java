package loops;

public class Sorting {

	public static void main(String[] args) {

		int[] arr = { 26, 10, 15, 6, 45, 23, 45, 13, 90, 94, 91 };

		////// { 3,6, 8, 3, 23, 45 }
		////// 1.For each element - go though the array
		////// 2.Compare each pair of the elements. If the next element is the next
		////// element is
		////// smaller than the current - swap them
		////// 3.Do the same for all the elements

		//// For the array { 3, 6, 8, 3, 45, 23 }
		//// 1. After the first loop - {3,6,3,8,23,45}
		//// 2. 2nd loop - {3,3,6,8,23,45}
		//// 3. 3nd loop - {3,3,6,8,23,45}
		//// 4. 4nd loop - {3,3,6,8,23,45}
		//// 4. 5nd loop - {3,3,6,8,23,45}

		
		
		//// Complexity is O((n-1)^2) = O(n^2)
		
		///In other programmin languages - there is the type of loop --> DO n TIMES
		for (int i = 0; i < arr.length - 1; i++) {//// Points us 1) How many pairs to be checked
			///// 2) The last position to stop
			for (int j = 0; j < arr.length - i - 1; j++) { //// The cycle which goes through the elements
/////compare the current to the next ==> the current must be less than the last element - 1 

				//// Change from > to < will sort the array in descending order
				if (arr[j] > arr[j + 1]) {///// In the declaration of the inner loop we have "- 1", because here we have
					///// arr[j+1], which will be the last element, when j is arr.length - i - 2
					//// How to swap two elements:
					int var = arr[j];
					arr[j] = arr[j + 1];
					arr[j + 1] = var;
				}
			}
		}

		//// This algorithm works the same way as above - because the last i elements
		//// will already be sorted
//		for (int i = 0; i < arr.length - 1; i++) {
//			for (int j = 0; j < arr.length - 1; j++) {
//				if (arr[j] > arr[j + 1]) {
//					int var = arr[j];
//					arr[j] = arr[j + 1];
//					arr[j + 1] = var;
//				}
//			}
//		}

		for (int element : arr) {
			System.out.print(element);
			System.out.print(" ");
		}

	}

}
