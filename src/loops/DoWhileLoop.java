package loops;

public class DoWhileLoop {

	public static void main(String[] args) {

		int val = 2;

		do
			val++;
		while (val < 2);

//		while (val < 2)
//			val++;

		System.out.println(val);

	}

}
