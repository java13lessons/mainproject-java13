package loops;

public class Matrixes {

	public static void main(String[] args) {

		//// The must have condition to multiply the matrixes is - the number of the
		//// colummns in the first matrix is the same as
		/// the number of the rows in the second matrix
		int matrix1[][] = { { 3, 5, 6 }, { 6, 4, 3 }, { 5, 3, 7 } };
		int matrix2[][] = { { 2, 7, 3 }, { 4, 6, 3 }, { 2, 5, 2 } };

		//// When we multiply two matrixes --> the result matrix will have the same
		//// number of the
		/// rows as the first matrix does, and the same number the columns the second
		//// matrix does

		// Determine the length of the rows and columns in the resulting matrix
		int rows = matrix1.length;
		int columns = matrix2[0].length;
		int result[][] = new int[rows][columns];/// { {0,0,0,0,...,0}, {0,0,....,0},...,{0,0,...,0} }

		////// Multiplication
		for (int i = 0; i < rows; i++) {///// Go though all the rows (in the resulting matrix)
			for (int j = 0; j < columns; j++) {///// Go though all the columns (in the resulting matrix)
				for (int ij = 0; ij < matrix1[i].length; ij++) {//// Is needed to fill all the values for the resulting
																//// matrix
					result[i][j] += (matrix1[i][ij] * matrix2[ij][j]);
				}
			}
		}

		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(result[i][j]);
				System.out.print(" ");
			}
			System.out.print(System.lineSeparator());
		}

		//// Sum the matrixes
		/// Must have condition - the dimensions of both matrixes must be the same
		for (int i = 0; i < rows; i++) { /// All the rows
			for (int j = 0; j < columns; j++) { /// All the columns
				result[i][j] = matrix1[i][j] + matrix2[i][j];
			}
		}

		System.out.print(System.lineSeparator());
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				System.out.print(result[i][j]);
				System.out.print(" ");
			}
			System.out.print(System.lineSeparator());
		}

	}

}
