package loops;

public class Iterate {

	public static void main(String[] args) {

		int[] values = new int[5]; /// 1,2,3,4,5

		/// i++ == i = i + 1;

		for (int i = 0; i < 5; i++) { //// (variable; boolean; action, which happens AFTER each iteration
			values[i] = i + 1;
		}

//		for (int i = 0; true; i++) {  ////(variable; boolean; action, which happens AFTER each iteration
////			values[i] = i + 1;
//			System.out.println("Endless loop");
//		}

//		for (int i = 0; i < 5; i++)
//			System.out.println(values[i]); ///print the values of the array

		//// ENDLESS LOOP
//		for (;;)
//			System.out.println("Here");

//		for (int i = 7; i < 10; i++)
//			System.out.println(i);///// print the values of i

//		for (int i = 1; i < 10; i *= 2)
//			////at each iteration i = i * 2
//			/// if we start with i = 0 ==> i will always be 0, because 0 times 2 = 0
//			System.out.println(i);///// print the values of i

//		System.out.println(5.0 / 2.0);/// 2.5
//      System.out.println(5 / 2);/// 2, because .5 is cut
//		for (int i = 10; i > 0; i /= 2)
//			//// at each iteration i = i * 2
//			/// if we start with i = 0 ==> i will always be 0, because 0 times 2 = 0
//			System.out.println(i);///// print the values of i

//		for (int i = 0; i < 10; i += 2) {
//			i--;
//			System.out.println(i);
//		}

		int[] studentsMarks = new int[100];

		///// Assign random mark from 1 to 10 to each of 100 students
		for (int i = 0; i < 100; i++)
			studentsMarks[i] = (int) (Math.random() * 10) + 1;//// range is 0-9 + 1 => 1-10

//		for (int i = 0; i < studentsMarks.length; i += 2)
//			System.out.println(studentsMarks[i]); /// print out all the marks

		//// Start with the last element and go to the first
		for (int i = studentsMarks.length - 1; i > -1; i--)
			System.out.println(studentsMarks[i]); /// print out all the marks
	}

}
