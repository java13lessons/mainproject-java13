package loops;

public class ForEach {

	public static void main(String[] args) {
		int[] arr = { 3, 65, 43, 93 };

		for (int value : arr) {
			value += 2;
			System.out.println(value);
		}
	}

}
