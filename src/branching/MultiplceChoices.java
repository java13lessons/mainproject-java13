package branching;

public class MultiplceChoices {

	public static void main(String[] args) {

		int number = 6;
		char grade; /// if number = 4 --> D, if number = 5----> C; if number 6 ---> B if number 7
					/// ---->A

//		if (number == 4)
//			grade = 'D';
//		else if (number == 5)
//			grade = 'C';
//		else if (number == 6)
//			grade = 'B';
//		else if (number == 7)
//			grade = 'A';
//		else
//			grade = ' ';

		switch (number) {
		case 4:
			grade = 'D';
			break;
		case 5:
			grade = 'C';
			break;
		case 6:
			grade = 'B';
			break;
		case 7:
			grade = 'A';
			break;
		default:
			grade = ' ';
			break;
		}

		System.out.println("Grade is " + grade);

	}

}
