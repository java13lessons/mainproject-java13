package branching;

public class Trick {

	public static void main(String[] args) {

		int age = 15;
		int votes = 321321321;
		int votesLeft = 44324324;

		System.out.println("Let's vote");

		////// Can be used, instead of putting all the code under if statement
		if (!(age >= 18)) {
			return;
		}

		System.out.println("Your vote is submited!");
		votes++;
		votesLeft--;

	}

}
