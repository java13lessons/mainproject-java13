package branching;

public class Actions {

	public static void main(String[] args) {
		
		boolean sunnyDay = true;
		boolean haveBusTicket = true;

		if (sunnyDay == true) //// if true -> go the Beach and finish the action
			goToBeach();
//
//		if (haveBusTicket == true)
//			moveToAnotherPlace();
		else if (haveBusTicket == true)
			moveToAnotherPlace(); //// if sunnyDay == false -> move to another place
									/// and finish the action
		else
			goShopping(); //// if nothing above is true -> goShopping

//		if (sunnyDay = false) /// don't use single =
//			goToBeach();

	}

	public static void goToBeach() {
		System.out.println("We are on the Beach");
	}

	public static void goShopping() {
		System.out.println("We are in the Shopping Center");
	}

	public static void moveToAnotherPlace() {
		System.out.println("Searching for sun");
	}

}
