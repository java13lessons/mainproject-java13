package branching;

import java.util.Scanner;

public class DateCalculation {

	public static void main(String[] args) {
		int day, year, januaryDays, februaryDays, marchDays, aprilDays, mayDays, juneDays, julyDays, augustDays,
				septemberDays, octoberDays, novemberDays, decemberDays;
		String month = "";
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the day of the year: ");
		int dayOfTheYear = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter the year: ");
		year = Integer.parseInt(myScanner.nextLine());

		if (year < 0) {
			System.out.println("Year cannot be negative");
			myScanner.close();
			return;
		}

		int modulo = year % 4;

		if (modulo == 0)
			februaryDays = 29;
		else
			februaryDays = 28;

		januaryDays = 31;
		februaryDays += januaryDays;
		marchDays = februaryDays + 31;
		aprilDays = marchDays + 30;
		mayDays = aprilDays + 31;
		juneDays = mayDays + 30;
		julyDays = juneDays + 31;
		augustDays = julyDays + 31;
		septemberDays = augustDays + 30;
		octoberDays = septemberDays + 31;
		novemberDays = octoberDays + 30;
		decemberDays = novemberDays + 31;

		if (dayOfTheYear < 1) {
			System.out.println("Day is incorrect");
			myScanner.close();
			return;
		}

		if (dayOfTheYear <= januaryDays) {
			month = "January";
			day = dayOfTheYear;
		}
		/// 32 ---> 1st of February ==> 1 = 32 - 21;
		else if (dayOfTheYear > januaryDays && dayOfTheYear <= februaryDays) {
			month = "February";
			day = dayOfTheYear - januaryDays;
		} else if (dayOfTheYear > februaryDays && dayOfTheYear <= marchDays) {
			month = "March";
			day = dayOfTheYear - februaryDays;
		} else if (dayOfTheYear > marchDays && dayOfTheYear <= aprilDays) {
			month = "April";
			day = dayOfTheYear - marchDays;
		} else if (dayOfTheYear > aprilDays && dayOfTheYear <= mayDays) {
			month = "May";
			day = dayOfTheYear - aprilDays;
		} else if (dayOfTheYear > mayDays && dayOfTheYear <= juneDays) {
			month = "June";
			day = dayOfTheYear - mayDays;
		} else if (dayOfTheYear > juneDays && dayOfTheYear <= julyDays) {
			month = "July";
			day = dayOfTheYear - juneDays;
		} else if (dayOfTheYear > julyDays && dayOfTheYear <= augustDays) {
			month = "August";
			day = dayOfTheYear - julyDays;
		} else if (dayOfTheYear > augustDays && dayOfTheYear <= septemberDays) {
			month = "Septemeber";
			day = dayOfTheYear - augustDays;
		} else if (dayOfTheYear > septemberDays && dayOfTheYear <= octoberDays) {
			month = "October";
			day = dayOfTheYear - septemberDays;
		} else if (dayOfTheYear > octoberDays && dayOfTheYear <= novemberDays) {
			month = "November";
			day = dayOfTheYear - octoberDays;
		} else if (dayOfTheYear > novemberDays && dayOfTheYear <= decemberDays) {
			month = "December";
			day = dayOfTheYear - decemberDays;
		} else {
			System.out.println("Day is incorrect");
			myScanner.close();
			return;
		}

		System.out.println("Select date formatting. 1- YYYY/MM/DD, 2-YYYY.MM.DD :");
		int formatOption = Integer.parseInt(myScanner.nextLine());

		switch (formatOption) {
		case 1:
			System.out.println(year + "/" + month + "/" + day);
			break;
		case 2:
			System.out.println(year + "." + month + "." + day);
			break;
		default:
			System.out.println("Option " + formatOption + " does not exist!");
			break;
		}

		myScanner.close();
	}

}
