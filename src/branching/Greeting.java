package branching;

public class Greeting {

	public static void main(String[] args) {
		int time =  16;
		String greeting;

		if (time < 0 || time > 24) {
			System.out.println("Input " + time + " is not valid, please, correct!");
			return;
		}

		if (time <= 12)
			greeting = "Good Morning Sunshine";
		else if (time >= 13 && time <= 19)
			greeting = "Good Afternoon. Work Hard!";
		else if (time >= 20 && time <= 24)
			greeting = "Good Evening. Get some rest!";
		else
			greeting = "";

		System.out.println(greeting);
	}

	public static void anyMethod() {
		return; /// finish the exececution of the method here
	}

}
