package branching;

import java.util.Scanner;

public class GuessTheNumber {

	public static void main(String[] args) {

		int randomNumber = (int) Math.round(Math.random() * 10);

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Guess the number below:");
		int number = myScanner.nextInt();

		if (number == randomNumber)
			System.out.println("Correct!");
		else
			System.out.println("Correct number is " + randomNumber);

		myScanner.close();
	}
}
