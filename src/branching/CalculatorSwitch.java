package branching;

import java.util.Scanner;

public class CalculatorSwitch {
	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter operator ^,*, /, + or -");
		char operator = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		double number2 = myScanner.nextDouble();

		double result;

		switch (operator) {
		case '*':
			result = number1 * number2;
			break;
		case '/':
			result = number1 / number2;
			break;
		case '+':
			result = number1 + number2;
			break;
		case '-':
			result = number1 - number2;
			break;
		case '^':
			result = Math.pow(number1, number2);
			break;
		default:
			System.out.println("You've entered " + operator + ", which is incorrect");
			myScanner.close();
			return;
		}

		System.out.println("Result is " + result);
		myScanner.close();
	}
}
