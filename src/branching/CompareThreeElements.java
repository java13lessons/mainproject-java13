package branching;

public class CompareThreeElements {

	public static void main(String[] args) {
		short var1 = -6, var2 = 203, var3 = 32;

		if (var1 > var2) {

			if (var3 > var1)
				System.out.println(
						"Number " + var3 + " is larger than " + var1 + " and " + var1 + " is larger than " + var2);
			else if (var3 < var2)
				System.out.println(
						"Number " + var1 + " is larger than " + var2 + " and " + var2 + " is larger than " + var3);
			else
				System.out.println(
						"Number " + var1 + " is larger than " + var3 + " and " + var3 + " is larger than " + var2);

		} else if (var1 < var2) {

			if (var3 > var2)
				System.out.println(
						"Number " + var3 + " is larger than " + var2 + " and " + var2 + " is larger than " + var1);
			else if (var3 < var1)
				System.out.println(
						"Number " + var2 + " is larger than " + var1 + " and " + var1 + " is larger than " + var3);
			else
				System.out.println(
						"Number " + var2 + " is larger than " + var3 + " and " + var3 + " is larger than " + var1);

		} else {

			if (var3 < var2)
				System.out.println(
						"Variables" + var1 + " and " + var2 + "are the same, number " + var3 + " is the smallest one");
			else
				System.out.println(
						"Variables" + var1 + " and " + var2 + "are the same, number " + var3 + " is the largest one");
		}
		if (var1 % 2 == 1)
			System.out.println("The number " + var1 + " is odd");
		else
			System.out.println("The number " + var1 + " is even");
//		else if (var2 % 2 == 1) -- THIS IS INCORRECT!
		if (var2 % 2 == 1)
			System.out.println("The number " + var2 + " is odd");
		else
			System.out.println("The number " + var2 + " is even");

		if (var1 < 0)
			System.out.println("Number " + var1 + " is negative");
		else if (var1 > 0)
			System.out.println("Number " + var1 + " is positive");

		if (var2 < 0)
			System.out.println("Number " + var2 + " is negative");
		else if (var2 > 0)
			System.out.println("Number " + var2 + " is positive");

		if (var1 > 100)
			System.out.println("Number " + var1 + " is greater than 100");

		if (var2 > 100)
			System.out.println("Number " + var2 + " is greater than 100");
	}

}
