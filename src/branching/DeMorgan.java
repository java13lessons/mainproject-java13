package branching;

public class DeMorgan {

	public static void main(String[] args) {
		boolean condition1 = true;
		boolean condition2 = false;

		// Both if statements are the same
		if (!(condition1 || condition2)) {
		}

		if (!condition1 && !condition2) {
		}

		// Both if statements are the same
		if (!(condition1 && condition2)) {
		}

		if (!condition1 || !condition2) {
		}

	}

}
