package branching;

import java.util.Scanner;

public class SalaryCalculation {

	public static void main(String[] args) {

		int workingHoursInDay;
		int salary;
		final int HOUR_RATE_STANDARD = 10;
		final int HOUR_RATE_OVERWORK = 15;

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter the working hours:");
		workingHoursInDay = Integer.parseInt(myScanner.nextLine());

		if (workingHoursInDay > 24 || workingHoursInDay < 0) {
			System.out.println("Working hours are incorrect");
			return;
		}

		if (workingHoursInDay <= 8)
			salary = workingHoursInDay * HOUR_RATE_STANDARD;
		else
			salary = (8 * HOUR_RATE_STANDARD) + ((workingHoursInDay - 8) * HOUR_RATE_OVERWORK);

		System.out.println("The salary is " + salary + " EUR");

	}

}
