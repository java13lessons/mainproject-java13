package branching;

import java.util.Scanner;

import basics.Country;

public class Date {

	public static void main(String[] args) {
		int day, month, year;
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the day: ");
		day = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter the month: ");
		month = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter the year: ");
		year = Integer.parseInt(myScanner.nextLine());

		boolean correct = true;

		if (year < 0) {
			System.out.println("Year cannot be negative");
			return;
		}

//		if (month < 1 || month > 12) {
//			System.out.println("Month is incorrect");
//			return;
//		}

		if (day < 1) {
			System.out.println("Day is incorrect");
			return;
		}

		switch (month) {
		case 1:
			if (day > 31)
				correct = false;
			break;
		case 2:
			int modulo = year % 4;

			if (modulo == 0 && day > 29)
				correct = false;
			else if (modulo != 0 && day > 28)
				correct = false;
			break;
		case 3:
			if (day > 31)
				correct = false;
			break;
		case 4:
			if (day > 30)
				correct = false;
			break;
		case 5:
			if (day > 31)
				correct = false;
			break;
		case 6:
			if (day > 30)
				correct = false;
			break;
		case 7:
			if (day > 31)
				correct = false;
			break;
		case 8:
			if (day > 31)
				correct = false;
			break;
		case 9:
			if (day > 30)
				correct = false;
			break;
		case 10:
			if (day > 31)
				correct = false;
			break;
		case 11:
			if (day > 30)
				correct = false;
			break;
		case 12:
			if (day > 31)
				correct = false;
			break;
		default:
			System.out.println("Month is incorrect");
			return;
		}

		if (correct == false) {///if correct is occurred (it is true)
			/////// !correct (!false is true) and false == false is true 
			System.out.println("Day is incorrect!");
			return;
		}

		System.out.println("Select date formatting. 1- YYYY/MM/DD, 2-YYYY.MM.DD :");
		int formatOption = Integer.parseInt(myScanner.nextLine());

		switch (formatOption) {
		case 1:
			System.out.println(year + "/" + month + "/" + day);
			break;
		case 2:
			System.out.println(year + "." + month + "." + day);
			break;
		default:
			System.out.println("Option " + formatOption + " does not exist!");
			break;
		}

	}

}
