package branching;

public class StringIssue {

	public static void main(String[] args) {

		String abc = "abc";
		String abc2 = "a";
		String abc3 = "bc";
		abc2 += abc3;

		System.out.println(abc);
		System.out.println(abc2);

//		if (abc == abc2)///// don't use for Strings!
//      if(abc.equals(abc2))//// use this one

		System.out.println(abc == abc2);
		System.out.println(abc.equals(abc2));
	}

}
