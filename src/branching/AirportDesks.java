package branching;

import java.util.Scanner;

public class AirportDesks {

	public static String checkedInCompnay = "";

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter your destination:");
		String destination = myScanner.nextLine();

		switch (destination) {
		case "Berlin":
			checkIn("Lufthansa");
			break;
		case "Helsinki":
			checkIn("FinAir");
			break;
		case "Tallin":
			checkIn("AirBaltic");
			break;
		case "Oslo":
			checkIn("SAS");
			break;
		default:
			System.out.println("No airline available");
			break;
		}

		myScanner.close();
	}

	public static void checkIn(String airline) {
		AirportDesks.checkedInCompnay = airline;
		System.out.println("Thanks! You've been checked in " + airline + "!");

	}

}
