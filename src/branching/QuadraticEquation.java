package branching;

import java.util.Scanner;

public class QuadraticEquation {

	public static void main(String[] args) {

		//// ax^2+bx+c = 0

		System.out.println("Solve equation ax^2+bx+c=0");
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter a:");
		double a = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter b:");
		double b = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter c:");
		double c = Double.parseDouble(myScanner.nextLine());

		double D = Math.pow(b, 2) - (4 * a * c);
		if (D < 0) {
			System.out.println("Solution does not exist in real numbers");
			return;
		}

		double x1 = ((-1 * b) + Math.sqrt(D)) / (2 * a);
		double x2 = ((-1 * b) - Math.sqrt(D)) / (2 * a);

		System.out.println("Solution is : x1 = " + x1 + "; x2 = " + x2);

	}

}
