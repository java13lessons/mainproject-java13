package branching;

public class Exam {

	public static void main(String[] args) {

		boolean test1Passed = true;
		boolean test2Passed = true;
		boolean test3Passed = false;

		if ((test1Passed && test2Passed) || test3Passed) {
			System.out.println("Exam is passed!");
		} else {
			System.out.println("Exam is not passed!");
		}
		
	}

}
