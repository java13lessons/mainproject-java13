package mathoperationsexamples;

public class Operators {

	public static void main(String[] args) {
		int a = 4;
		a++;/// a = a + 1;
		System.out.println(a);

		a = 4;
		a--;/// a = a - 1;
		System.out.println(a);

		a = 4;
		a += 3; // a = a + 3;
		/// = is not EQUALS!!!!
		/// = this assigment operator
		System.out.println(a);
		/// at this point a equals 7

		a *= 2;// a = 2*a;
//		a = 2*a;
		System.out.println(a);

		/// a equals 14

		//// If a is greater than 3, then result is 4 otherwise result is 5
		int result = (a > 3) ? 4 : 5;
		System.out.println(result);

		result = (a > 19) ? 4 : 5;
		System.out.println(result);

		System.out.println(a > 19);
		System.out.println(a >= 14);
		System.out.println(a < 19);
		System.out.println(a <= 19);/// returns boolean value
		System.out.println(a == 15); // a equals to 15?
		System.out.println(a == 14); // a equals to 14?
		System.out.println(a = 15); // Now a is 15!!
		System.out.println(a == 15); // a equals to 15?
		System.out.println(a == 14); // a equals to 14?
		System.out.println(a != 14); // a not equals to 14?
		System.out.println(!(a == 14)); // not (a equals to 14)?

		System.out.println(a % 7); /// remaining part of the division : a
		// divided by 7. a - a / 7 (get integer). 15 / 7 = 2 15 = 2 * 7 + 1

		int b = 4;
		int c = 8;
		boolean var = true;

		System.out.println((b == c) || (var == true));
		System.out.println((b == c) && (var == true));
		System.out.println((b == c) || (var == false));
		System.out.println((b == c) || !(var == false));

		// a - (-4) = a + 4
		/// !(true) == false; !(false) == true

//		System.out.println(++c); ++c --> 9
		System.out.println(c++); // ++c --> 8
		System.out.println(c); /// c == 9

		System.out.println(Math.PI);

		final double EXPONENT;
		EXPONENT = 2.71;

		// = 2.71; // Constant
//		EXPONENT = 7; CANNOT ASSIGN A NEW VALUE!!!
		
		System.out.println(Math.abs(-132.23));
		System.out.println(Math.sqrt(9));
		System.out.println(Math.pow(3, 4));
		System.out.println(Math.round(3.8));
		System.out.println(Math.round(3.3));
		
	}

}
