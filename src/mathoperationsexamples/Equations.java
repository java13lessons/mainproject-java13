package mathoperationsexamples;

public class Equations {

	public static void main(String[] args) {
		System.out.println("f1(34) = " + f1(34)); /// the same as Equations.f1(34)
		System.out.println("f2(4.5,3.6,9.3,3) = " + f2(4.5, 3.6, 9.3, 3));
		System.out.println("f3(5.8,9.4) = " + f3(5.8, 9.4));
		solveEquation();
		System.out.println("Random group : " + getGroupNumber(329832));
	}

	public static double f1(double x) {
		return Math.pow(x, 3) + (3 * Math.pow(x, 2)) - (5 * x) + 7;
	}

	public static double f2(double x, double y, double z, int n) {
		return Math.pow(x, 2) + (7 * Math.pow(y, 2)) - (n * z);
	}

	public static double f3(double x, double y) {
		return (2 * Math.pow(x, 4)) + (4 / y) + Math.sqrt(y) + Math.cbrt(x) + Math.abs(y - x);
	}

	public static void solveEquation() {
		double a = 1;
		double b = 5;
		double c = 4;
		double D = Math.pow(b, 2) - (4 * a * c);
		double x1 = (-b + Math.sqrt(D)) / (2 * a);
		double x2 = (-b - Math.sqrt(D)) / (2 * a);

		System.out.println("x1 = " + x1 + ", x2 = " + x2);
	}

	public static int getGroupNumber(int id) {
//		 Math.random(); /// random value 0-1 -> 0.23; 0.76; 1; 0;....
		double randomNumber = Math.random() * id; /// -> from 0 to id
		int randomNoInt = (int) randomNumber;
		return randomNoInt % 6; /// in the range from 0 to 5;
	}

}
