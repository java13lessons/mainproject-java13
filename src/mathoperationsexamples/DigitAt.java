package mathoperationsexamples;

import java.util.Scanner;

public class DigitAt {
	public static void main(String[] args) {

		System.out.println("Enter the number:");
		Scanner myScanner = new Scanner(System.in);
		int number = Integer.parseInt(myScanner.nextLine());

		System.out.println("Enter the position:");
		myScanner = new Scanner(System.in);
		int position = Integer.parseInt(myScanner.nextLine());

		int len = (int) (Math.log10(number) + 1);

		int subNumber = number % (int) Math.pow(10, len - position);
		double divisionValue = subNumber / (int) Math.pow(10, len - position - 1);

		///Ex. 839747873 / 10^8 = 8.39747873. floor(8.39747873) = 8;
		
		/// floor(3.8) ---> 3
		int digitAtPosition = (int) Math.floor(divisionValue);
		System.out.println(digitAtPosition);

		///// 32132131231 <----- number
		///// 6
		///// should return 3

	}
}
