package arrays;

public class Intro {

	public static void main(String[] args) {

		int varInt = 4;
		String name = "Name";
		int arr[] = { 10, 34, 53, 23, 32 };

		System.out.println(arr.length);
		System.out.println(arr);

		int temperatureArray[] = { 16, 20, 21, 23, 22 };
		System.out.println(temperatureArray[0]);
		temperatureArray[2] = 43;
		System.out.println(temperatureArray[2]);
		temperatureArray[5] = 20;

	}

}
