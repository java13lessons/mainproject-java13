package arrays;

public class ArrayReference {

	public static void main(String[] args) {
		String[] array1 = new String[2];

		array1[0] = "Name1";
		array1[1] = "Name2";

		String[] array2;
		array2 = array1; ///// REMEMBER - AFTER THIS array1 is the same array as array2!!!

		System.out.println(array1[1]);
		array2[1] = "Name3";
		System.out.println(array1[1]);

		System.out.println(array1 == array2);

		int[] arr1 = { 2, 3 };
		int[] arr2 = { 2, 3 };
		System.out.println(arr1 == arr2);

	}

}
