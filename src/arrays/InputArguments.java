package arrays;

public class InputArguments {

	public static void main(String[] args) {

		if (args.length > 0)
			System.out.println(args[0]);

		if (args.length > 1)
			System.out.println(args[1]);

		if (args.length > 2)
			System.out.println(args[2]);

		if (args.length > 3)
			System.out.println(args[3]);
	}

}
