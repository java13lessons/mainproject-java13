package arrays;

public class TwoDimensionalArrays {

	public static void main(String[] args) {

		int[][] array2D = new int[2][4];

		array2D[0][0] = 1;
		array2D[0][1] = 2;
		array2D[0][2] = 3;
		array2D[0][3] = 4;

		array2D[1][0] = 1;
		array2D[1][1] = 2;
		array2D[1][2] = 3;
		array2D[1][3] = 4;

		int secondArray2D[][] = { { 1, 2, 3, 4 }, { 1, 2, 3, 4, 7 } };
		System.out.println(secondArray2D[1].length);

	}

}
