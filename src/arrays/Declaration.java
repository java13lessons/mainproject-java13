package arrays;

public class Declaration {

	public static void main(String[] args) {

		boolean isValueDifferent = false;
		//// Object is NOT the same as variable!!!!!
		int[] arr;//// the variable
		arr = new int[5]; // the Object/the array is created and passed to the variable
//		int arr[] = { 10, 34, 53, 23, 32 };

		arr[0] = 10;
		arr[1] = 34;
		arr[2] = 53;

		if (!isValueDifferent)
			arr[3] = 23;
		else
			arr[3] = 34;

		arr[4] = 32;

		System.out.println(arr[4]);

		System.out.println(arr[arr.length - 1]);//// access the last element of the array

//		arr = new int[7]; ////new array / object is created

	}

}
