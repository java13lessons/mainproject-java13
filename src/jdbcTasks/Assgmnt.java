package jdbcTasks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Assgmnt {

	private static Connection conn;

	public static void main(String[] args) throws Exception {

		Class.forName("com.mysql.jdbc.Driver");
		Assgmnt.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
//		fillCategories();
//		fillSuppliers();
//		fillStatuses();
//		fillProducts();
//		fillOrders();
//		updateOrders();
//		deleteDuplicate();
		printAllOrders();
	}

	private static void printAllOrders() throws SQLException{
		///// Print all Orders and use the corresponding names for Products and Statuses
		Statement statement = Assgmnt.conn.createStatement();
		/// Execute select query
		ResultSet resultOrders = statement
				.executeQuery("SELECT java13.products.name, java13.orders.quantity, java13.orders.cust_name,"
						+ " java13.orders.cust_surname, java13.orders.cust_email, java13.orders.cust_phone_number, java13.statuses.name FROM java13.orders JOIN "
						+ " java13.products ON java13.products.id = java13.orders.product_id JOIN java13.statuses ON java13.statuses.id = java13.orders.status_id"
						+ " ORDERED BY java13.statuses.name");
		/// Print all the results
		while (resultOrders.next()) {
			System.out.print("Product name: " + resultOrders.getString(1) + ",");
			System.out.print("Quantity: " + resultOrders.getInt(2) + ", ");
			System.out.print("Customer name: " + resultOrders.getString(3) + ",");
			System.out.print("Customer surname: " + resultOrders.getString(4) + ",");
			System.out.print("Customer e-mail: " + resultOrders.getString(5) + ",");
			System.out.print("Customer phone: " + resultOrders.getString(6) + ",");
			System.out.print("Order status: " + resultOrders.getString(7) + ",");
			System.out.print(System.lineSeparator());
		}
	}

	private static void deleteDuplicate() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute("DELETE FROM java13.products WHERE description = 'Computer from Apple'");

	}

	private static void updateOrders() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();

		/// Close all the orders which are in progress

		/// Here we need to get the name (text) of the status, based on the id stored in
		/// Orders table
		//// Since we need to find the id for the status "in progress", "delivered", no
		/// need for Map here

		int inProgressStatusID = 0, deliveredStatusID = 0;
		ResultSet resultStatuses = statement.executeQuery("SELECT * FROM java13.statuses");
		/// Find the ID for the statuses - in progress, delivered
		while (resultStatuses.next()) {
			if (resultStatuses.getString("name").equals("in processing"))
				inProgressStatusID = resultStatuses.getInt("id");
			else if (resultStatuses.getString("name").equals("delivered"))
				deliveredStatusID = resultStatuses.getInt("id");
		}

		// Mark the Orders, which must be updated
		List<Integer> idsToUpdate = new ArrayList<Integer>();
		ResultSet resultOrders = statement.executeQuery("SELECT * FROM java13.orders");
		while (resultOrders.next()) {
			if (resultOrders.getInt("status_id") == inProgressStatusID)
				idsToUpdate.add(resultOrders.getInt("id"));

		}

		/// Go through all the Order, which are marked to be updated
		Iterator<Integer> iterator = idsToUpdate.iterator();
		while (iterator.hasNext())
			statement.execute(
					"UPDATE java13.orders SET status_id = " + deliveredStatusID + " WHERE id = " + iterator.next());

	}

	private static void fillOrders() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();

		/// Get the mapping name -> id for products and statuses
		Map<String, Integer> productMap = new TreeMap<String, Integer>();
		ResultSet resultProducts = statement.executeQuery("SELECT * FROM java13.products");
		while (resultProducts.next())
			productMap.put(resultProducts.getString("name"), resultProducts.getInt("id"));

		Map<String, Integer> statusMap = new TreeMap<String, Integer>();
		ResultSet resultStatuses = statement.executeQuery("SELECT * FROM java13.statuses");
		while (resultStatuses.next())
			statusMap.put(resultStatuses.getString("name"), resultStatuses.getInt("id"));

		/// Place the orders
		/// Create 3 orders
		statement.execute(
				"INSERT INTO java13.orders (product_id,quantity,cust_name,cust_surname,cust_email,cust_phone_number,status_id) "
						+ "VALUES ('" + productMap.get("MacBook")
						+ "',1,'J�nis','Liepi��','j.l@gmail.com','+371243276','" + statusMap.get("in processing")
						+ "')");

		statement.execute(
				"INSERT INTO java13.orders (product_id,quantity,cust_name,cust_surname,cust_email,cust_phone_number,status_id) "
						+ "VALUES ('" + productMap.get("Windows 10")
						+ "',2,'Zigmurds','Liepi��','z.l@gmail.com','+371233676','" + statusMap.get("delivered")
						+ "')");

		statement.execute(
				"INSERT INTO java13.orders (product_id,quantity,cust_name,cust_surname,cust_email,cust_phone_number,status_id) "
						+ "VALUES ('" + productMap.get("Capital")
						+ "',1,'Zigmurds','K�avi��','z.k@gmail.com','+371213776','" + statusMap.get("canceled") + "')");

	}

	private static void fillProducts() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		//// Need to get the categories first

		/// To map Category name to category id
		Map<String, Integer> categoriesMap = new TreeMap<String, Integer>();
		ResultSet resultCategories = statement.executeQuery("SELECT * FROM java13.categories");
		/// For test, let's print the result query (categories)
		while (resultCategories.next()) {
			categoriesMap.put(resultCategories.getString("name"), resultCategories.getInt("id"));
//			///We can provide the column name (id, name)
////			System.out.print("ID : " + resultCategories.getInt("id") + " ");
////			System.out.print("Category name: " + resultCategories.getString("name"));
//			
//			//With the same result, we can provide the column index/position (starting at 1)
//			System.out.print("ID : " + resultCategories.getInt(1) + " ");
//			System.out.print("Category name: " + resultCategories.getString(2));
//			System.out.print(System.lineSeparator());
//			
		}

//		///The same with the statuses
//		Map<String, Integer> statusesMap = new TreeMap<String, Integer>();
//		ResultSet resultStatuses = statement.executeQuery("SELECT * FROM java13.statuses");
//		while(resultStatuses.next()) 
//		  statusesMap.put(resultStatuses.getString("name"), resultStatuses.getInt("id"));	

		/// The same with the supplier
		Map<String, Integer> suppliersMap = new TreeMap<String, Integer>();
		ResultSet resultSuppliers = statement.executeQuery("SELECT * FROM java13.suppliers");
		while (resultSuppliers.next())
			suppliersMap.put(resultSuppliers.getString("name"), resultSuppliers.getInt("id"));

		/// Create 3 products
		statement.execute("INSERT INTO java13.products (name,description,price,warranty,category_id,supplier_id) "
				+ "VALUES ('MacBook','Computer Apple',1500,2,'" + categoriesMap.get("computers") + "','"
				+ suppliersMap.get("Supplier2") + "')");

		statement.execute("INSERT INTO java13.products (name,description,price,warranty,category_id,supplier_id) "
				+ "VALUES ('Windows 10','Software Microsoft',100,2,'" + categoriesMap.get("software") + "','"
				+ suppliersMap.get("Supplier3") + "')");

		statement.execute("INSERT INTO java13.products (name,description,price,warranty,category_id,supplier_id) "
				+ "VALUES ('Capital','This is PC',500,1,'" + categoriesMap.get("PCs") + "','"
				+ suppliersMap.get("Supplier1") + "')");

	}

	private static void fillCategories() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute("INSERT INTO java13.categories (name) VALUES ('portable')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('computers')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('PCs')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('software')");
		statement.execute("INSERT INTO java13.categories (name) VALUES ('accessories')");
	}

	private static void fillSuppliers() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier1','+371254755','supplier1@gmail.com')");
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier2','+371254775','supplier2@gmail.com')");
		statement.execute(
				"INSERT INTO java13.suppliers (name,phone_number,email) VALUES ('Supplier3','+371235775','supplier3@gmail.com')");

	}

	private static void fillStatuses() throws Exception {
		Statement statement = Assgmnt.conn.createStatement();
		statement.execute("INSERT INTO java13.statuses (name) VALUES ('entered')");
		statement.execute("INSERT INTO java13.statuses (name) VALUES ('in processing')");
		statement.execute("INSERT INTO java13.statuses (name) VALUES ('canceled')");
		statement.execute("INSERT INTO java13.statuses (name) VALUES ('delivered')");
	}

}
