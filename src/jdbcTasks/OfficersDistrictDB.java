package jdbcTasks;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class OfficersDistrictDB {

	protected static Connection conn;/// protected values can be accessed from the classes which are located in the
										/// same packaged

	public static void main(String[] args) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
		OfficersDistrictDB.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
		OfficersDistrictDB.conn.setAutoCommit(false);//// no changes will automatically be saved in the database, before
														//// we commit the changes

		fillDistricts();
		OfficersDistrictDB.conn.commit();/// to commit the changes
//	OfficersDistrictDB.conn.rollback();///to rollback all the changes, which are not commited
		OfficersDistrictDB.conn.close();
	}

	private static void fillDistricts() throws Exception {
		/// In this case we use two values as the key
		Map<NameSurname, Officer> officers = new TreeMap<NameSurname, Officer>();

		java.util.Scanner sc = new java.util.Scanner(System.in);

		System.out.println("How many officers to add?");
		int numberOfOfficers = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < numberOfOfficers; i++) {
			System.out.println("Enter name:");
			String name = sc.nextLine();
			System.out.println("Enter surname:");
			String surname = sc.nextLine();
			Officer officer = new Officer(name, surname);
			officer.addOfficerToDatabase();
			officers.put(new NameSurname(name, surname), officer);
		}

		System.out.println("How many districts to add?");
		int numberOfDistricts = Integer.parseInt(sc.nextLine());
		for (int i = 0; i < numberOfDistricts; i++) {
			System.out.println("Enter district name:");
			String name = sc.nextLine();
			System.out.println("Enter city:");
			String city = sc.nextLine();

			District district = new District(name, city);
			district.addDistrictToDatabase();
			System.out.println("How officers there are in the District?");
			int numberOfTheOfficersInDistrict = Integer.parseInt(sc.nextLine());
			for (int j = 0; j < numberOfTheOfficersInDistrict; j++) {
				System.out.println("Name of the officer?");
				String officerName = sc.nextLine();
				System.out.println("Surname of the officer?");
				String officerSurname = sc.nextLine();
				NameSurname namesurname = new NameSurname(officerName, officerSurname);
				if (!officers.containsKey(namesurname)) {//// Check if the officer with such name and surname is created
					System.err.println("The officer does not exist");
					continue;/// We go to the next iteration
				}
				System.out.println("How many crimes in district are solved?");
				int crimesSolved = Integer.parseInt(sc.nextLine());

				Officer officer = officers.get(namesurname);
				district.addOfficer(officer, crimesSolved);

			}
			district.fillOfficersInDitrictDB();
		}

		sc.close();
	}
}

class Officer {
	/// Since name and surname will be filled only once, we declare them as final
	private final String name, surname;
	private int id;

	public Officer(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addOfficerToDatabase() throws Exception {
		PreparedStatement statement = OfficersDistrictDB.conn.prepareStatement(
				"INSERT INTO java13.officers (Name,Surname) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, this.name);
		statement.setString(2, this.surname);
		statement.executeUpdate();
		ResultSet idSet = statement.getGeneratedKeys();
		idSet.next();/// set the iterator for the first and the only entry
		this.setId(idSet.getInt(1));
	}

}

class District {
	/// Since name and city will be filled only once, we declare them as final
	private final String name, city;
	private int id;
	private Map<Officer, Integer> officersInDistrict = new HashMap<Officer, Integer>();

	public void addOfficer(Officer officer, int crimesSolved) {
		this.officersInDistrict.put(officer, crimesSolved);
	}

	public District(String name, String city) {
		this.city = city;
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public String getName() {
		return name;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void addDistrictToDatabase() throws Exception {
		PreparedStatement statement = OfficersDistrictDB.conn.prepareStatement(
				"INSERT INTO java13.districts (name,city) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, this.name);
		statement.setString(2, this.city);
		statement.executeUpdate();
		ResultSet idSet = statement.getGeneratedKeys();
		idSet.next();/// set the iterator for the first and the only entry
		this.setId(idSet.getInt(1));
	}

	public void fillOfficersInDitrictDB() throws Exception {
		/// Fill DB table officers_in_district
		Iterator<Officer> iterator = this.officersInDistrict.keySet().iterator();
		while (iterator.hasNext()) {
			Officer officer = iterator.next();
			int crimesSolved = this.officersInDistrict.get(officer);
			PreparedStatement statement = OfficersDistrictDB.conn
					.prepareStatement("INSERT INTO java13.officers_in_districts "
							+ "(officer_id,district_id,crimes_solved_in_district) VALUES (?,?,?)");
			statement.setInt(1, officer.getId());
			statement.setInt(2, this.id);
			statement.setInt(3, crimesSolved);
			statement.execute();
		}
	}

}

////Need two fields/attributes as the keys for the Map
class NameSurname implements Comparable<NameSurname> {
	private String name, surname;

	@Override
	public int compareTo(NameSurname o) {
		String name1 = this.surname + this.name;
		String name2 = o.getSurname() + o.getName();
		return name1.compareTo(name2);
	}

	public NameSurname(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

}
