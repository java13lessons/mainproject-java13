package jdbcTasks;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Map;
import java.util.TreeMap;

public class CompanyDB {

	protected static Connection conn;
	private static java.util.Scanner sc;

	public static void main(String[] args) throws Exception {
		sc = new java.util.Scanner(System.in);
		Class.forName("com.mysql.jdbc.Driver");
		CompanyDB.conn = DriverManager.getConnection("jdbc:mysql://127.0.0.1/", "root", "");
		CompanyDB.conn.setAutoCommit(false);

		try {
			Map<NameSurname, Employee> employees = new TreeMap<NameSurname, Employee>();
			Map<String, Role> roles = new TreeMap<String, Role>();

			/// 1st table - table of the Employees
			System.out.println("Migrate employees");
			String employeesCSV = getFileContent();
			String[] employeesCsvData = employeesCSV.split(System.lineSeparator());
			for (int i = 1; i < employeesCsvData.length; i++) {/// we start at the second line, because the first line
																/// is
																/// the header line (names of the columns)
				String[] nameSurname = employeesCsvData[i].split(",");
				Employee employee = new Employee(nameSurname[0], nameSurname[1]);
				employees.put(new NameSurname(nameSurname[0], nameSurname[1]), employee);
				employee.addEmployeeToDatabase();
			}

			/// 2nd table - table of the Roles
			System.out.println("Migrate Roles");
			String rolesCSV = getFileContent();
			String[] rolesCsvData = rolesCSV.split(System.lineSeparator());
			for (int i = 1; i < rolesCsvData.length; i++) {
				String roleName = rolesCsvData[i];
				Role role = new Role(roleName);
				roles.put(roleName, role);
				role.addRoleToDatabase();
			}
			/// 3rd
			System.out.println("Migrate linkages between employees and roles");
			String linkagesCsv = getFileContent();
			String[] linkagesCsvData = linkagesCsv.split(System.lineSeparator());
			for (int i = 1; i < linkagesCsvData.length; i++) {
				String[] linkageColumns = linkagesCsvData[i].split(",");
				String employeeName = linkageColumns[0];
				String employeeSurname = linkageColumns[1];
				String employeeRole = linkageColumns[2];
				int employeeExperience = Integer.parseInt(linkageColumns[3]);
				addLinkageToDatabase(employees.get(new NameSurname(employeeName, employeeSurname)).getId(),
						roles.get(employeeRole).getId(), employeeExperience);
			}

			CompanyDB.conn.commit();
		} catch (Exception e) {
			e.printStackTrace();
			CompanyDB.conn.rollback();
		}
		CompanyDB.conn.close();
		sc.close();
	}

	private static void addLinkageToDatabase(int employeeID, int roleID, int experince) throws Exception {
		PreparedStatement statement = CompanyDB.conn
				.prepareStatement("INSERT INTO java13_company.employees_roles (employee_id,role_id,experience_years) "
						+ "VALUES (?,?,?)");
		statement.setInt(1, employeeID);
		statement.setInt(2, roleID);
		statement.setInt(3, experince);
		statement.execute();
	}

	private static String getFileContent() throws Exception {
		System.out.println("Enter the path to the file:");

		String path = sc.nextLine();
		File fileCSV = new File(path);
		BufferedReader reader = new BufferedReader(new FileReader(fileCSV, StandardCharsets.UTF_8));
		StringBuffer strBuild = new StringBuffer();

		String line = reader.readLine(); // read the first line
		while (line != null) { /// check if we haven't reached the end file (null - no more lines)
			strBuild.append(line); /// append the line to out "final" result
			strBuild.append(System.lineSeparator());/// also need to add the line separator, to go to the next line
			line = reader.readLine();/// read next line
		}
		reader.close();
		return strBuild.toString();
	}

}

class Role {
	private int id;
	private String name;

	public Role(String name) {
		this.name = name;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	private void setId(int id) {
		this.id = id;
	}

	public void addRoleToDatabase() throws Exception {
		PreparedStatement statement = CompanyDB.conn.prepareStatement(
				"INSERT INTO java13_company.roles (name) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, this.name);
		statement.executeUpdate();
		ResultSet idSet = statement.getGeneratedKeys();
		idSet.next();/// set the iterator for the first and the only entry
		this.setId(idSet.getInt(1));
	}
}

class Employee {
	private int id;
	private String name;
	private String surname;

	public Employee(String name, String surname) {
		this.name = name;
		this.surname = surname;
	}

	public int getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	private void setId(int id) {
		this.id = id;
	}

	public void addEmployeeToDatabase() throws Exception {
		PreparedStatement statement = CompanyDB.conn.prepareStatement(
				"INSERT INTO java13_company.employees (name,surname) VALUES (?,?)", Statement.RETURN_GENERATED_KEYS);
		statement.setString(1, this.name);
		statement.setString(2, this.surname);
		statement.executeUpdate();
		ResultSet idSet = statement.getGeneratedKeys();
		idSet.next();/// set the iterator for the first and the only entry
		this.setId(idSet.getInt(1));
	}

}
