package arrayLists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class GeneralTypeStorage {

	public static void main(String[] args) {

		List<Employee> employees = new ArrayList<Employee>();
		Company company = new Company();
		company.setEmployees(employees);

		Developer developer1 = new Developer();
		developer1.setNumberOfSkills(23);
		Developer developer2 = new Developer();
		developer2.setNumberOfSkills(12);

		Tester tester1 = new Tester();
		Tester tester2 = new Tester();

		/// We can add Developers and Testeters to the list, because both these types
		/// (class) are inherited from the class Employee
		employees.add(tester1);
		employees.add(tester2);
		employees.add(developer1);
		employees.add(developer2);

		System.out.println("The average level of skills is " + company.avgLevelOfSkills());

	}

}

class Company {

	private List<Employee> employees;

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public float avgLevelOfSkills() {
		/// Since we all the employees are stored in the list, we don't know exactly how
		/// many developers there are
		int levelTotal = 0, numberOfDevelopers = 0;
		Iterator<Employee> iterator = this.employees.iterator();
		while (iterator.hasNext()) {
			Employee employee = iterator.next();
			/// Need to find if the employee is Developer or not

			// If the check below is skipped, then ClassCastException will be met
			if (employee instanceof Developer) {// return true if Employee is Developer, returns false otherwise
				Developer developer = (Developer) employee; /// here we cast Employee to Developer
				numberOfDevelopers++;/// we found the developer, so we can increament the total number of the
										/// developers
				levelTotal += developer.getNumberOfSkills();
			}

		}
		return (float) levelTotal / (float) numberOfDevelopers;
	}
}

class Employee {

}

class Developer extends Employee {

	private int numberOfSkills;

	public void setNumberOfSkills(int numberOfSkills) {
		this.numberOfSkills = numberOfSkills;
	}

	public int getNumberOfSkills() {
		return numberOfSkills;
	}

}

class Tester extends Employee {

}
