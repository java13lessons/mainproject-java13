package arrayLists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class FindMaxValue {

	public static void main(String[] args) {
		/// At first, we create all the students and set the info of them
		Student student1 = new Student();
		student1.setGrade(8);
		student1.setName("Phil");
		Student student2 = new Student();
		student2.setGrade(5);
		student2.setName("Robert");
		Student student3 = new Student();
		student3.setGrade(9);
		student3.setName("Jimmy");
		Student student4 = new Student();
		student4.setGrade(7);
		student4.setName("John");

		/// Here we create the arraylist and put all the students there
		List<Student> students = new ArrayList<Student>();
		students.add(student1);
		students.add(student2);
		students.add(student3);
		students.add(student4);

///Find the best student
		Student bestStudent = null;
		Iterator<Student> iterator = students.iterator();
		while (iterator.hasNext()) {
			Student currentStudent = iterator.next();
			if (bestStudent == null || bestStudent.getGrade() < currentStudent.getGrade())/// if the grade of the
																							/// current student is
																							/// greater than the grade
																							/// of the best student,
																							/// than we know that the
																							/// current student is the
																							/// best one at this point
																							/// of loop
				bestStudent = currentStudent;
		}

		System.out.println(bestStudent.getName() + " is the best student");

	}

}

class Student {

	private int grade;
	private String name;

	public int getGrade() {
		return grade;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setGrade(int grade) {
		this.grade = grade;
	}

}
