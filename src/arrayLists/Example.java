package arrayLists;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Example {

	public static void main(String[] args) {
//		Object[] arr = new Object[10];  in this case any object can be placed to the array
//		int[] arr = new int[]; ///cannot be created with providing the number of the elements

		/// ArrayList - that's the type, <Integer> - it means that ONLY Integers can be
		/// stored in this arraylist

//		ArrayList<Object> - if we provide <Object> as the type of the arraylist - 
		//// it means that ALL the types of the elements can be stored in the Arraylist

		///// We usually not specify the full type of the List, when we declare the
		///// variable
		//// List is interface, and ArrayList is the class
//		List<Object> arrList = new ArrayList<Object>();////in fact this means that we can place any object inside the array list
		List<Integer> arrList = new ArrayList<Integer>();/// the same as for all the Objects - need to call new
		/// (need to create the object)
//		arrList.add("String");/////since we defined <Integer> as 
		///// the type of the elements to be stored in the arraylist, we cannot store
		///// the elements with other types

//		ArrayList<int> arrListInt; /// primitives CANNOT be used as the types of the elements in arraylists!
		///// REMARK: We cannot provide primitive types in ALL the cases (all the
		///// classes) where generics <....> are used!

		arrList.add(4343);
		arrList.add(423432432); /// we can add as many integers as we want
		arrList.add(1232132);
		arrList.add(653);
///List will look like ----> 4343, 423432432,...
//		Example.printArrayList(arrList);
		arrList.add(2, 1);/// adds the element between the elements located at the positions 2 and 3
		Example.printArrayList(arrList);
//		arrList.addAll(......)// as the argument we provide another ArrayList (with Integers), 
		//// then all these elements will be added to the arrList. Please, check the
		//// usage in assgmntOffcer.Execution class
//		arrList.clear(); // all the elements are deleted from the list
		System.out.println(arrList.contains(1));//// checks if the element 1 exists in the list
		System.out.println(arrList.contains(2));/// false, because there is no such element added to the list
//		arrList.containsAll(c)//checks if there are multiple elemetns in the list
		arrList.remove(3); // this will remove the element positioned at index 3
	}

	////// The list - elements stored one by one
	private static void printArrayList(List<Integer> list) {
		/// how to iterate through the arraylists?
		///// Actually, there are 2 options
		//// 1) Simple option (we already used) - using for loop. BUT THIS IS NOT HOW WE
		/// USUALLY ITERATE TROUGH THE COLLECTION
//		for (int i = 0; i < list.size(); i++)
//			System.out.println(list.get(i)); //// using get(i) method - we can get the element positioned at index i
		/// 2) Using iterator (default option for the collections)
		Iterator<Integer> iterator = list.iterator(); /// we declare the variable and get the iterator from the list
		// Now we can go through the list
		while (iterator.hasNext())/// if there is the next element in the list, hasNext() will return true
			System.out.println(iterator.next()); /// using iterator.next() - we can get the next element
	}

}
