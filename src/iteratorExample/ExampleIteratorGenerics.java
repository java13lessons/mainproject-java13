package iteratorExample;

import java.util.List;

////For generics we provide the types to be used <type1, type2,....>
//// Naming convetion - T as type, K as key, etc. Mostly we use T - the type
public class ExampleIteratorGenerics<T> {
///In all the places where T is used. once the variable which is refered 
///to the class ExampleIteratorGenerics is declared with the type provided,
///then in all the places in the class, where T is used, it will be "replaced" with the type provided 

	private int position = -1;
	private List<T> col;

	public boolean hasNext() {
		if (col.size() > this.position + 1)
			return true;
		else
			return false;
	}

	//// For example, if we define the variable ExampleIteratorGenerics<String>
	//// ...., then T will be considered as String
	public T next() {
		this.position += 1;/// increase the position, since we want to get the next element
		return this.col.get(this.position);
	}

	public ExampleIteratorGenerics(List<T> col) {
		this.col = col;
	}

}
