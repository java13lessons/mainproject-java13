package iteratorExample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
//import java.util.*; ///import classes/interfaces from java.util package

public class TestIterator {

	public static void main(String[] args) {
		List<Object> col = new ArrayList<Object>();//// we can always String to Object
		//// we can cast everything to object
		col.add("Name1");
		col.add("Name2");
		col.add("Name43");

		/// This is our custom iterator
		ExampleIterator iterator = new ExampleIterator(col);
		while (iterator.hasNext())
			iterator.next();

		/// This is java created iterator. Result is the same

		Iterator<Object> iteratorJava = col.iterator();
		while (iteratorJava.hasNext()) {
			Object element = iteratorJava.next();
			//// we can do any action with the elment here
		}
//		Object element = iteratorJava.next();/// if next element does not exist - corresponing excpetion will occur

//		List<Lawyer> colPers = new ArrayList<Lawyer>();
//		/// Create different persons and add them collection. There won't be any values
//		/// under these objects
//		colPers.add(new Lawyer());
//		colPers.add(new Lawyer());
//		colPers.add(new Lawyer());
//		colPers.add(new Lawyer());
//		Iterator<Lawyer> iteratorLawyers = colPers.iterator();
//		int totalCrimes = 0;
//		while (iteratorLawyers.hasNext()) {
//			Lawyer lawyer = iteratorLawyers.next();//// get the person from the collection
////			person.getContact(); ///we can call any method from the person object
//			totalCrimes += lawyer.getHelpedinCrimesSolvin();
//		}

	}

}
