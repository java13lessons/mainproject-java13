package iteratorExample;

import java.util.ArrayList;
import java.util.List;

public class IteratorGenericsTest {

	public static void main(String[] args) {

		List<String> col = new ArrayList<String>();//// we can always String to Object
		//// we can cast everything to object
		col.add("Name1");
		col.add("Name2");
		col.add("Name43");

		ExampleIteratorGenerics<String> iterator = new ExampleIteratorGenerics<String>(col);
		while (iterator.hasNext())
			System.out.println(iterator.next());

	}

}
