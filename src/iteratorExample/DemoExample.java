package iteratorExample;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DemoExample {

	public static void main(String[] args) {
		List<Integer> integers = new ArrayList<Integer>();
		integers.add(12);
		integers.add(53);
		integers.add(93);
		integers.add(15);
		integers.add(76);

//		integers.forEach(number -> System.out.println(number));
		Iterator<Integer> iterator = integers.iterator();
		while (iterator.hasNext()) {
			int intVal = iterator.next();
		}

	}

}
