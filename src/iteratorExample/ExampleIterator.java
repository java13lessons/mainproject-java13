package iteratorExample;

import java.util.List;

/// Generics are used for the iterator classes, will skip skip such option in this test example
public class ExampleIterator {

	private int position = -1;//// the initial before we start
	//// -1 is the value usually used to describe null (nothing). In this case it
	//// describes the position of the collection which does not exist
	private List<Object> col;/// if we use Object - remember it means that all the reference types can be
								/// added to the collection

	/// Check if there is next element in the collection
	public boolean hasNext() {
		if (col.size() > this.position + 1) // using size() we can check if there is the element at position
											// this.position + 1
			return true;
		else
			return false;
	}

	public Object next() {
		this.position += 1;/// increase the position, since we want to get the next element
		return this.col.get(this.position);
	}

	public ExampleIterator(List<Object> col) {
		this.col = col;
	}

}
