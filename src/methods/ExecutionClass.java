package methods;

public class ExecutionClass {

	public static void main(String[] args) {
		Parent obj = new Subclass();
		obj.testMethod();
		PropertyIneterface itrfce = obj;
		itrfce.testMethod();
	}

}
