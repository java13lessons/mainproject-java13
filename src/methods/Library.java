package methods;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {

	private List<Book> booksRegistered = new ArrayList<Book>();
	private List<Book> booksAvailable = new ArrayList<Book>();

	public void addBook(Book book) {
		this.booksAvailable.add(book);
		this.booksRegistered.add(book);
	}

	public void borrowBook(String name) {
		Iterator<Book> iterator = this.booksAvailable.iterator();
		int indexFound = -1;/// position in the collection the book we are searching is located
		int i = -1;/// use i to determine the position
		while (iterator.hasNext()) {
			i++;
			Book currentBook = iterator.next();
			if (currentBook.getName().equals(name)) {
				//// Borrow it
				indexFound = i;
				break;
			}

		}
		if (indexFound != -1)
			this.booksAvailable.remove(indexFound);
		else
			System.out.println("Book is not found/ not available");
	}

	public void returnBook(String name) {
		Iterator<Book> iterator = this.booksRegistered.iterator();
		Book book = null;
		while (iterator.hasNext()) {
			Book currentBook = iterator.next();
			if (currentBook.getName().equals(name)) {
				//// Book registered found
				book = currentBook;
				break;
			}

		}
		if (book != null)
			this.booksAvailable.add(book);
		else
			System.out.println("Book is not registered in the library");
	}

	public static void main(String[] args) {
		Library library = new Library();

		Book book1 = new Book("Harry Potter", "J.K. Rowling");
		library.addBook(book1);

		Book book2 = new Book("The Little Prince", "Antoine de Saint-Exup�ry");
		library.addBook(book2);

		Book book3 = new Book("Dream of the Red Chamber", "Cao Xueqin");
		library.addBook(book3);

		/// Borrow the book
		/// Find and Borrow Harry Potter
		library.borrowBook("Harry Potter");

		/// Borrow the book
		/// Find and Borrow The Little Prince
		library.borrowBook("The Little Prince");

		/// Return the book
		library.returnBook("The Little Prince");
	}

}

class Book {
	private String name;
	private String author;

	public Book(String name, String author) {
		this.author = author;
		this.name = name;
	}

	public String getAuthor() {
		return author;
	}

	public String getName() {
		return name;
	}

}
