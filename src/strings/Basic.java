package strings;

public class Basic {

	public static void main(String[] args) {
		char[] text = { 'A', 'r', 't', 'u', 'r', 's' };
		String textStr = "Arturs";
		textStr = new String("Arturs");/// the same as above

		///// "a" <- this is the string, which has one element/character 'a'
		///// 'a' <- this is the character

		System.out.println("lengh() -> " + textStr.length());// the lengh of the string
		System.out.println("charAt(0) -> " + textStr.charAt(0));// returns the first character
		System.out.println("charAt(3) -> " + textStr.charAt(4));// returns the fourth character

		for (int i = 0; i < textStr.length(); i++)
			System.out.print(textStr.charAt(i));/// print all the characters of the String
		System.out.print(System.lineSeparator());

		System.out.println(textStr);/// in fact, this print is done in the same way, as the loop above

		String name1 = "%#john";
		System.out.println(name1.matches("^[a-zA-Z]+"));//// check if only the letters are used

		int variable = 45;
		String intvalue = String.format("%d", variable); // for integers - need to use %d as the format
		System.out.println(intvalue);

		float fValf = 45.4f;
		String fValStr = String.format("%.2f", fValf); // for floats - need to use %f as the format
		System.out.println(fValStr);

		double dVald = 25.43453;
		String dValStr = String.format("%.2f", dVald); // for doubles - the same as for floats
		System.out.println(dValStr);

		String combination = dVald + " " + fValf + " <- these are the values";
		System.out.println(combination);

		String name = "John";
		byte age = 18;
		char gradeLevel = 'A';
		String course = "JAVA";

		String formatedString = String.format("My name is %s and I am %d years old. I have %c in the %s", name, age,
				gradeLevel, course);

		System.out.println(formatedString);

		String formatedString2 = String.format("The text, value1 = %.2f, value2 = %d", 32.43423, 91);
		System.out.println(formatedString2);

		/////// FOR ALL THE REFERENCE TYPES:
		/////// 1) Variable stores the references to the memory (also called "objects")
		/////// 2) Reference stores the values
		String a = "Abcd";
		String b = "efg";
		String c = "Abcdefg";
		String ab = a + b;
		System.out.println(ab);
		System.out.println(ab == c);////// here we compare the references (stored under the variables)
		System.out.println(ab.equals(c));///// here we actually the content, stored under the references

		///Null pointer exception is produced below
//		String nullString = null;
//		nullString.charAt(0);

	}

}
