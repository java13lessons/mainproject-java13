package strings;

import java.util.Scanner;

public class Regex {

	public static void main(String[] args) {
		Scanner sc = new java.util.Scanner(System.in);
//		System.out.println("Enter the e-mail:");
//		String emailentered = sc.nextLine();
//
//		//// . is metacharacter. To use dot as the character, we can use [.]
//
//		//////// For the emails we have recipient (Any letters uppercase/lowercase) <---
//		//////// [a-zA-Z0-9]+, then we have @ symbol <---@, then we have domain name
//		//////// (lowercase only) <----[a-z0-9]+, then we have dot <--- [.] and then we
//		//////// have top level domain, [a-z]+.
//		if (emailentered.matches("[a-zA-Z0-9]+@[a-z0-9]+[.][a-z]+"))
//			System.out.println("E-Mail is correct");
//		else
//			System.out.println("E-Mail is incorrect");

		System.out.println("Enter the IBAN:");
		String ibanNumber = sc.nextLine();
		if (ibanNumber.matches("[A-Z]{2}[0-9]{2}[A-Z]{4}[0-9]{13}"))
			System.out.println("IBAN is correct");
		else
			System.out.println("IBAN is incorrect");

		sc.close();
	}

}
