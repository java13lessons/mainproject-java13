package strings;

public class Asgmnt2 {

	public static void main(String[] args) {

//		String srtTes = "Abbccdd";
//		System.out.println(srtTes.replace("b", "o"));//// all b characters are replaced with o
//		System.out.println(srtTes.replace("c", ""));//// all c characters are replaced with empty character (c
//													//// characters are removed from the string)

		String str = "Climb mountains not so the world can see you, but so you can see the world";
//		String str = "Column1;Column2;Column3"; ////We can use split method ex. to process csv files, where the values at each columns are separated by separator/delimeter
//		String[] arr = str.split(";");
		String[] arr = str.split(" ");
		System.out.println("The are " + arr.length + " word in the string");
//		printStringArray(arr); //just to print the array, to check

		int countThe = 0;
		for (String val : arr) {
			if (val.replace(",", "").replace(":", "").replace(";", "").equals("the"))
				countThe++;
		}
		System.out.println("There are " + countThe + " words the in the sentence");

		int countS = 0;
		for (String val : arr) {
			if (val.contains("s"))
				countS++;
		}
		System.out.println("There are " + countS + " words which contain s letter");

		/// 1) We go though all the elements of the array (using for .. i .. cycle)
		/// 2) For each element of the first loop we check all the elements of the array
		/// (which are indexed > i, because we already checked all the elements
		/// indexed < i)
		for (int i = 0; i < arr.length; i++)
			for (int j = i + 1; j < arr.length; j++)
				if (arr[i].replace(",", "").replace(":", "").replace(";", "")
						.equals(arr[j].replace(",", "").replace(":", "").replace(";", "")))
					System.out.println("Word " + arr[i].replace(",", "").replace(":", "").replace(";", "")
							+ " is repeated multiple times");

		System.out.println(str.replace("you", "You")); /// we can use replace for the single character and for the
														/// substring as well

	}

	private static void printStringArray(String[] arr) {
		for (String str : arr)
			System.out.println(str);
	}

}
