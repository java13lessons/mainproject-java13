package strings;

public class Asgmn3 {

	public static void main(String[] args) {
//		String name = "John432432";
		String name = "John"; //// will be true
//		String pattern = "[a-zA-Z]+"; ////only letters allowed
		String pattern = "[A-Z]{1}[a-z]+";/// the first letter must be in uppercase, all the other - in lowercase
		///// for regex {1} - defines the lenght of the character sequence
		///// [a-z]+ means any (unlimited) number of character of the letters a-z / any
		///// lenght
		System.out.println(name.matches(pattern));

		String username = "s20surname";
		pattern = "[A-Za-z]{1}[0-9]{2}[A-Za-z]+";
		System.out.println(username.matches(pattern));

		String personCodeOfLativa = "121200-11311";
		pattern = "[0-9]{6}-[0-9]{5}";
		System.out.println(personCodeOfLativa.matches(pattern));

	}

}
