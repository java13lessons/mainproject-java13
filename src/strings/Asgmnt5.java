package strings;

public class Asgmnt5 {

	public static void main(String[] args) {
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the sentence:");
		String sentence = sc.nextLine();
		
		/// If we want to ignore the characters like ?, comma, semicolon,etc. - then we
		/// can use the method replace
//		System.out.println(sentence.replace(" ", "").replace(",", "").equalsIgnoreCase(reverse(sentence)));

		/// using the method replaceAll(regex,replacement)
		/// in the case below - ^ - means NOT. Please, note there is no lenght or +
		/// defined, that's because we process SINGLE characters
		System.out.println(sentence.replaceAll("[^a-zA-Z0-9]", "").equalsIgnoreCase(reverse(sentence)));
	}

	/// The method which returns reversed string
	private static String reverse(String str) {
		//// String stringValue = str; ///that's simple, we just pass the value to the
		//// variable
		//// String stringValue = str + "StringAnother"; /// here we actually a new
		//// String / new Object, which takes more resources for the program

		/// If we need to mutate the String - we should normally use StringBuilder or
		/// StringBuffer
		StringBuilder strBuild = new StringBuilder(str.replaceAll("[^a-zA-Z0-9]", ""));
		strBuild = strBuild.reverse();
		return strBuild.toString();
	}

}
