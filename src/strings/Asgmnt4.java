package strings;

public class Asgmnt4 {

	enum cardSuits {
		SPADES, CLUBS, HEARTS, DIAMONDS
	};

	enum cardValues {
		TWO, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
	}

	public static void main(String[] args) {
		///// using the method values we can fetch the values of the enum as the array
		///// (enum --> array)
		String[][] cardDeck = new String[cardSuits.values().length][cardValues.values().length];
//Since we need to fill the array cardDeck - we need to know the indexes
		for (int i = 0; i < cardSuits.values().length; i++)
			for (int j = 0; j < cardValues.values().length; j++)
				cardDeck[i][j] = cardSuits.values()[i].name() + "-" + cardValues.values()[j].name();

		//// Shuffle
		/// The different ways how to implement random shuffle

		/// We will go though each of the deck and for each element we will calculate
		/// random number from 1 to 52 and we will swap the elements of the deck

//		final int numberOfTheCards = cardSuits.values().length * cardValues.values().length; // get the number of the
		// cards in the deck
		// we use final, because the number won't be changed

		for (int i = 0; i < cardSuits.values().length; i++)
			for (int j = 0; j < cardValues.values().length; j++) {
				// the approach below would be fine, if we have just one array of the elements
				// with the lenght 52, since we have 2D we need to get 2 random indexes
//				int swapPosition = (int) (Math.random() * numberOfTheCards) + 1;//////we get random position of the deck
				/// we swap two cards
				int swapSuite = (int) (Math.random() * cardSuits.values().length);
				int swapValue = (int) (Math.random() * cardValues.values().length);
				// swap the values
				String value = cardDeck[i][j];
				cardDeck[i][j] = cardDeck[swapSuite][swapValue];
				cardDeck[swapSuite][swapValue] = value;

			}

		System.out.println("The first card selected is: " + cardDeck[0][0]);

		for (int i = 1; i < 7; i++)
			System.out.println("The " + i + ".th card is " + cardDeck[0][i]);
//		print2DArray(cardDeck);
	}

	private static void print2DArray(String[][] arr) {
		for (int i = 0; i < arr.length; i++)
			for (int j = 0; j < arr[i].length; j++)
				System.out.println(arr[i][j]);
	}

}
