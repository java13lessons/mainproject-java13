package strings;

public class Asgmnt1 {

	public static void main(String[] args) {

		String[] arr = { "Computer", "Plate", "Chair", "Girl", "Boy", "Cat", "Dog", "Shirt", "Determination" };
		int countC = 0, countE = 0, countCh = 0, countLen5 = 0, countEs = 0, countTeSubStr = 0;

		for (String value : arr)
//			if (value.charAt(0) == 'C') we can use this approach
			if (value.startsWith("C")) /// we can use this one as well
				countC++;

		System.out.println("There are " + countC + " words starting with C");

		for (String value : arr)
			if (value.startsWith("Ch")) /// "check the string/ part of the string as well"
				countCh++;

		System.out.println("There are " + countCh + " words starting with Ch");

		for (String value : arr)
//			if (value.charAt(value.length() - 1) == 'e') we can use this approach
			if (value.endsWith("e")) /// we can use this one as well
				countE++;

		System.out.println("There are " + countE + " words ending with e");

		for (String value : arr)
			if (value.length() == 5) /// get length and compare it to 5
				countLen5++;

		System.out.println("There are " + countLen5 + " words, with length 5");

//		for (String value : arr)
//			if (value.contains("e"))
//				countEs++;

		/// does the same as the approach above
		for (String value : arr) {
			for (int i = 0; i < value.length(); i++)//// go through all the characters of the string
				if (value.charAt(i) == 'e') {/// if character e is occurred, increment countEs and break the current
												/// loop (where we go through all the characters of string
					countEs++;
					break;/// if break is reached - we finish/go out of the loop, we are currently in. NOT
							/// ALL THE LOOPS!!!
				}
		}

		System.out.println("There are " + countEs + " words contain character 5");

		for (String value : arr)
			if (value.contains("te"))
				countTeSubStr++;

		System.out.println("There are " + countTeSubStr + " words, which contain substring te");

		int[] lens = new int[30];///// for this scenario, we won't have the strings with more than 30 characters

		for (String value : arr)
			lens[value.length()]++;

		for (int i = 0; i < lens.length; i++)
			System.out.println(i + " - " + lens[i]);

	}

}
