package abstractClasses;

public class Rectangle extends Shape {

	private double width, height;

	public void setHeight(double height) {
		this.height = height;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public double getWidth() {
		return width;
	}

	@Override
	public double getArea() {
		return this.height * this.width;
	}

	@Override
	public double getPerimeter() {
		return 2 * (this.height + this.width);
	}

}
