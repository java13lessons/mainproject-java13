package abstractClasses;

public class Circle extends Shape {
//At first - all the abstract methods must be implemeneted!

	private double radiuss;

	public void setRadiuss(double radiuss) {
		this.radiuss = radiuss;
	}

	public double getRadiuss() {
		return radiuss;
	}

	@Override
	public double getArea() {
		return Math.PI * Math.pow(this.radiuss, 2);
	}

	@Override
	public double getPerimeter() {
		return 2 * Math.PI * this.radiuss;
	}

}
