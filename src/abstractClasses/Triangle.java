package abstractClasses;

public class Triangle extends Shape {

	private int edge1, edge2, edge3;
	private int heightAgainstEdge1;

	public int getEdge1() {
		return edge1;
	}

	public int getEdge2() {
		return edge2;
	}

	public int getEdge3() {
		return edge3;
	}

	public void setEdge1(int edge1) {
		this.edge1 = edge1;
	}

	public void setEdge2(int edge2) {
		this.edge2 = edge2;
	}

	public void setEdge3(int edge3) {
		this.edge3 = edge3;
	}

	public int getHeightAgainstEdge1() {
		return heightAgainstEdge1;
	}

	public void setHeightAgainstEdge1(int heightAgainstEdge1) {
		this.heightAgainstEdge1 = heightAgainstEdge1;
	}

	@Override
	public double getArea() {
		return 0.5 * this.edge1 * this.heightAgainstEdge1;
	}

	@Override
	public double getPerimeter() {
		return this.edge1 + this.edge2 + this.edge3;
	}

}
