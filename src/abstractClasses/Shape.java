package abstractClasses;

public abstract class Shape {

	abstract public double getArea();

	abstract public double getPerimeter();

//	abstract protected double getSomething(); /// it IS possible to define protected abstract method
//	abstract private double getSomething(); /// private abstract methods cannot be created

	///// For static methods - no differences comparing to standard classes
	public static void main(String[] args) {
//		Shape obj = new Shape(); /// cannot create the object from the abstract classes
	}

}
