package oop;

public class TestCasting {

	public static void main(String[] args) {
		SubClass var1 = new SubSubClass();/// up-casting
		MainClass var2 = var1;/// up-casting - we pass the object from variable which has the type (class)
								/// which is the subclass of the target variable (var2)

		// when we do the down-casting we always need to provide the type to which we
		// want to cast. The problem may be if the type of the object is the superclass
		// of the class the target variable is typed

		// Using ClassCastException catching we can catch incorrect casting
		try {
			var1 = (SubClass) var2;/// down-casting - we pass the object from the variable which has the type
			/// (class) which the superclass of the target variable (var1)
		} catch (ClassCastException e) {
		}
		/// For the variable typed/defined as the interfaces are the same as for the
		/// classes
		intFce varIntrf = var1;//// up-casting

		/// To not have the exception here, the object stored under the variable var1
		/// should implement interface intFce2

		intFce2 varIntf2;
		if (var1 instanceof intFce2)/// using intanceof keyword, we can check if cast can be done here
			varIntf2 = (intFce2) var1;/// down-casting
	}

}

interface intFce {
}

interface intFce2 {
}

class MainClass implements intFce {
}

class SubClass extends MainClass {
}

class SubSubClass extends SubClass implements intFce2 {
}
