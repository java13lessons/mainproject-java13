package oop;

public class PrivateClass {
//	ClassWithPrivateConstr obj = new ClassWithPrivateConstr();   cannot be done here, because the constructor is defined as private
	ClassWithPrivateConstr obj = ClassWithPrivateConstr.getInstance();

}

class ClassWithPrivateConstr {

	private ClassWithPrivateConstr() {

	}

	public static ClassWithPrivateConstr getInstance() {
		return new ClassWithPrivateConstr();
	}

}
