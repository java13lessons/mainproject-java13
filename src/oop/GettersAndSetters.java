package oop;

public class GettersAndSetters {

	public static void main(String[] args) {
		Employee obj = new Employee("Tim", "Cook", 51);
		System.out.println("The age is:" + obj.getAge());
		System.out.println("The name is:" + obj.getName());
		System.out.println("The surname is:" + obj.getSurname());
		System.out.print(System.lineSeparator());

		Employee obj2 = new Employee();
		if (obj2.setAge(-1) == 1)//// if value returned is 1, then error/issue occured
			System.out.println("The age can't be nagative!");
		obj2.setName("Bill4343");
		obj2.setSurname("Gates");
		System.out.println("The age is:" + obj2.getAge());
		System.out.println("The name is:" + obj2.getName());
		System.out.println("The surname is:" + obj2.getSurname());
	}

}

class Employee {
	private String name, surname;
	private int age;

	public int getAge() {
		return age;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

//	public void setAge(int age) {
//		if (age >= 0)
//			this.age = age;
//		else
//			System.out.println("The age can't be nagative!");
//	}

	/// return integer value (exit code) - 0 means the value is assigned, 1 means
	/// the value is not assigned
	public int setAge(int age) {
		if (age >= 0) {
			this.age = age;
			return 0;
		} else {
			return 1;
		}
	}

	public void setName(String name) {
		if (name.matches("[A-Za-z]+"))
			this.name = name;
		else
			System.out.println("The name must contain the letters only!");
	}

	public void setSurname(String surname) {
		if (surname.matches("[A-Za-z]+"))
			this.surname = surname;
		else
			System.out.println("The name must contain the letters only!");
	}

	public Employee() {
		// TODO Auto-generated constructor stub
	}

	public Employee(String name, String surname, int age) {
		this.age = age;
		this.name = name;
		this.surname = surname;
	}

}
