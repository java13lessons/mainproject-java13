package oop;

public class AccessScope {

	public static void main(String[] args) {

		int varMethod = 3;

		for (int i = 0; i < 10; i++) {
			int var = 4;
			varMethod = 3;//// it is possible
		}
//		var = 3; cannot be accessed
//		varMethod2 = 4;// can't access it as well
	}

	private static void method2() {
		int varMethod2 = 5;
	}

}
