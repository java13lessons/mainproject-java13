package oop;

public class Constructors {

	public static void main(String[] args) {
//		Person pers = new Person();
		///// At this point we have the object of Person, but there is no name assigned.
		///// What can we do to not allow the object of the Person to be create without
		///// the name?
		Person pers = new Person("Jack", "Johnson");
		/// At this point, when the object is created, the Object/Person will definetly
		/// have the name + surname

	}

}

class File {

	long createdAt;

	//// At the moment the File object is created, the system time is taken and
	//// stored under the object
	public File() {
		this.createdAt = System.currentTimeMillis();
	}

}

class Person {

	private String name, surname;

	/// By default - all the classes have the constructor and it is the method
	/// without any logic
//	public Person() { /// syntax for all the constructors - access modfier + classname + (arguments); no
//						/// returning type/no void
//	}

	//// This is the contructor, which asks to enter the name value
	public Person(String name, String surname) {//// if constructor does have the parameter, you cannot create the
												//// object,
		//// without providing it
		this.name = name;
		this.surname = surname;
	}

}
