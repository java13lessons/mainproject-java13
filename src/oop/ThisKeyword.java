package oop;

public class ThisKeyword {

	public static void main(String[] args) {

		WebSite wb = new WebSite();
		wb.setCreationYear(2021);
		wb.setHostname("Digital web Hosting");
		wb.setPort(34);
		wb.setUrl("www.mysite.com");
//		wb.showAttributes();
		WebSite returnedObject = wb.getMe();
		returnedObject.showAttributes();/// result will be the same, because returnedObject is the same as wb
		System.out.println(wb == returnedObject); /// wb and returnedObject have the same reference!

	}

}

class WebSite {

	private String url, hostname;
	private int port, creationYear;

	public void showAttributes() {
		System.out.println("The url: " + this.url);
		System.out.println("The hostname: " + this.hostname);
		System.out.println("The port: " + this.port);
		System.out.println("The website is created in " + this.creationYear);

	}

	public int getCreationYear() {
		return creationYear;
	}

	public void setCreationYear(int creationYear) {
		this.creationYear = creationYear;
	}

	public String getHostname() {
		return hostname;
	}

	public void setHostname(String hostname) {
		this.hostname = hostname;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public WebSite getMe() {
		///from the perspective of our programm this == wb (object declared in the main method above)
		return this;/// this will return the current object
	}

}
