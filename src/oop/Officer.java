package oop;

public class Officer implements Comparable<Officer> {

	private String name, surname, workingDistrict;
	private int officerID, crimesSolved;

	@Override
	/// This is the method, which returns negative, 0 or positive values
	/// negative (usually we use -1) means, that this < o (the object provided to
	/// the method)
	/// 0 - this == 0
	/// positive - this > o
	public int compareTo(Officer o) {
//		/// Let's compare the officers by crimes solved
//		if (this.crimesSolved < o.getCrimesSolved())
//			return -1; /// this < o
//		else if (this.crimesSolved == o.getCrimesSolved())
//			return 0; /// this == o
//		else
//			return 1; // this > o

		/// Compare by Name and Surname
		if (this.name.compareTo(o.getName()) > 0)
			return 1;
		else if (this.name.compareTo(o.getName()) < 0)
			return -1;
		else // names are the same, we can compare by surnames
			return this.surname.compareTo(o.getSurname()); /// this approach can be used to compare two strings
	}

	public static void main(String[] args) {
		Officer officer1 = new Officer();
		Officer officer2 = new Officer();
		Officer officer3 = new Officer();

		officer1.setCrimesSolved(15);
		officer1.setName("John");
		officer1.setSurname("Clain");
		officer1.setOfficerID(43626);
		officer1.setWorkingDistrict("Bruclin");

		officer2.setCrimesSolved(10);
		officer2.setName("Mike");
		officer2.setSurname("Cramer");
		officer2.setOfficerID(1426);
		officer2.setWorkingDistrict("Bruclin");

		officer3.setCrimesSolved(34);
		officer3.setName("Jason");
		officer3.setSurname("Kane");
		officer3.setOfficerID(2326);
		officer3.setWorkingDistrict("Bruclin");

		System.out.println("The first officer:");
		System.out.println(officer1);// if we pass the object to the println, then prints the value returned from
										// toString() method
		System.out.print(System.lineSeparator());

		System.out.println("The second officer:");
		System.out.println(officer2);
		System.out.print(System.lineSeparator());

		System.out.println("The third officer:");
		System.out.println(officer3);
		System.out.print(System.lineSeparator());

		Officer newOfficer = new Officer();
		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Enter the name:");
		newOfficer.setName(sc.nextLine());
		System.out.println("Enter the surname:");
		newOfficer.setSurname(sc.nextLine());
		System.out.println("Enter the Officer ID:");
		newOfficer.setOfficerID(Integer.parseInt(sc.nextLine()));
		System.out.println("Enter working district:");
		newOfficer.setWorkingDistrict(sc.nextLine());
		System.out.println("Enter crimes Solved:");
		newOfficer.setCrimesSolved(Integer.parseInt(sc.nextLine()));
		System.out.println(newOfficer);

		Officer[] district99 = new Officer[4];////// array is created here. NO OFFICER OBJECTS CREATED!!!
		district99[0] = officer1;
		district99[1] = officer2;
		district99[2] = officer3;
		district99[3] = newOfficer;

		int countLevel1 = 0;
//		int countLeverGreaterThan1 = 0;
		for (Officer officer : district99)
			if (officer.calculateLevel() == 1)
				countLevel1++;
//			else
//				countLeverGreaterThan1++;

		System.out.println("There are " + countLevel1 + " officers who have level 1");
//		System.out.println("There are " + countLeverGreaterThan1 + " officers who have level greater than 1");
		System.out
				.println("There are " + (district99.length - countLevel1) + " officers who have level greater than 1");/// the
																														/// result
		/// is the
		/// same as
		/// with the
		/// approach
		/// used
		/// above

		boolean JohnIsFound = false;

		for (Officer officer : district99)
			if (officer.getName().equals("John")) {
				JohnIsFound = true;
				break;// since John is found, we can go out of the loop (stop the loop)
			}

		// condition true/false ? (value if true) : (value if false)
		System.out.println(JohnIsFound ? "John is found" : "John is not found");
	}

	public Officer() {
		// TODO Auto-generated constructor stub
	}

	public Officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
		this.name = name;
		this.surname = surname;
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
		this.workingDistrict = workingDistrict;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getWorkingDistrict() {
		return workingDistrict;
	}

	public void setWorkingDistrict(String workingDistrict) {
		this.workingDistrict = workingDistrict;
	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimesSolved() {
		return crimesSolved;
	}

	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	//// by default - toString prints the address to the memory (the address of the
	//// reference)
	//// if we override it - it prints what we return from the method
	@Override
	public String toString() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator()
				+ "Officer ID: " + this.officerID + System.lineSeparator() + "Working district: " + this.workingDistrict
				+ System.lineSeparator() + "Crimes Solved: " + this.crimesSolved;
	}

	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;

	}

}
