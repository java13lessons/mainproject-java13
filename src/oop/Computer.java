package oop;

public class Computer {

	static String model;

	int weight, ram, screenSize, price;
	String color;

//	static void any() {
//	size = 4; //// NON-static arguments cannot be used in static methods
//	}

	///// IF THERE IS NO ACCESS MODIFIER (PRIVATE,PROTECTED OR PUBLIC) TYPED, THEN
	///// THE ACCESS MODIFIER IS CONSIDERED AS DEFAULT!
	void starting() {
//		size = 4; //// NON-static arguments can be used in non-static methods
//		var = 5; ////we can use static arguments and methods in non-static methods
		System.out.println("The computer is starting...");
	}

	void createExcelTable() {
		System.out.println("Excel table is created");
	}

	void playGame() {
		System.out.println("Playing the game");
	}

	/// Method, which displays all the attributes of the object
	void displayAttributes() {
		/// Ex.below - if you deaclare the variable with the same name as the attribute
		/// of the class, then inside the method the programm will use the variable you
		/// deaclare in the method
//		int price = 4;

		//// this keyword - it is the current object (we are currently inside)
		/// print out all the attributes
		System.out.println("Color: " + this.color);
		System.out.println("Price: " + this.price); /// you can skip using this keyword. The result will be the same,
													/// but with some exceptions...
		System.out.println("RAM: " + this.ram);
		System.out.println("Screeensize: " + this.screenSize);
		System.out.println("Weight: " + this.weight);
		System.out.println("Model: " + Computer.model); /// Java asks us to not access the static attribute using this.
		/// keyword
	}

	public static void main(String[] args) {

		/// Below - ONE Class and two objects, refered to the same class
		Computer comp1 = new Computer();//// using keyword 'new' - new object is created!
		Computer comp2 = new Computer(); /// here we create another object.

		//// We assign the attributes to our newly created object (computer)
		comp1.color = "Black";
		comp1.price = 500;
		comp1.ram = 16;
		comp1.screenSize = 15;
		comp1.weight = 1;
//		comp1.model = "Model1"; //this approach will be compiled and there won't be any errors (except the warning), 
		/// but the way is not quite correct, because we need to access static
		/// attributes from the class, not from the object, because the s
		/// static attributes belong to the class, not to the object.
		Computer.model = "Model1"; /// but actually will be the same if we access from then object (this way
									/// ->>comp1.model = "Model1";)

		/// We assign the values to the second computer (object)
		comp2.color = "Yellow";
		comp2.price = 700;
		comp2.ram = 32;
		comp2.screenSize = 13;
		comp2.weight = 1;
//		comp2.model = "Model2";
		Computer.model = "Model2";/// Since we assign the ne value here, the previous one will be replaced

		/// As we can see, the attribute model is never changed from the object, and
		/// that's it has no dependecies on the object properties!

		//// Print the content of the objects (Computers)
		System.out.println("The first computer: ");
		comp1.displayAttributes();

		System.out.print(System.lineSeparator());
		System.out.println("The second computer: ");
		comp2.displayAttributes();

		/// ABOVE: We actually have the common/the same logic placed in
		/// displayAttributes method, but the output is based on the content/properties
		/// and it can be different for different object

//		comp1.starting();
//		comp1.playGame();
//		comp1.createExcelTable();

//		AnotherClass obj = new AnotherClass();

//		obj.attribute2 = 2;

	}

}

class AnotherClass {

	private int attribute;/// cannot not be accessed from outside the class (ex. in
	public int attribute2;//// can be be accessed from outside the class

}
