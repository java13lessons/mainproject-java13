package oop;

public class Computer2 {
	public static void main(String[] args) {

		Comp comp1 = new Comp(1, 16, 15, 500, "Black");//// please, remember to put the attributes in the correct
														//// sequence!!
		Comp comp2 = new Comp(1, 32, 13, 700, "Yellow");

		System.out.println("The first computer: ");
		comp1.displayAttributes();

		System.out.print(System.lineSeparator());
		System.out.println("The second computer: ");
		comp2.displayAttributes();

		/// the result is the same as in the class Computer
	}
}

class Comp {

	private int weight, ram, screenSize, price;
	private String color;

	public Comp(int weight, int ram, int screenSize, int price) {

		this.weight = weight;/// mandatory need to use this keyword!
		this.ram = ram;
		this.price = price;
		this.screenSize = screenSize;
	}

	public Comp(int weight, int ram, int screenSize, int price, String color) {

		this.weight = weight;/// mandatory need to use this keyword!
		this.color = color;
		this.ram = ram;
		this.price = price;
		this.screenSize = screenSize;
	}

	/// Method, which displays all the attributes of the object
	void displayAttributes() {
		System.out.println("Color: " + this.color);
		System.out.println("Price: " + this.price); /// you can skip using this keyword. The result will be the
													/// same,
													/// but with some exceptions...
		System.out.println("RAM: " + this.ram);
		System.out.println("Screeensize: " + this.screenSize);
		System.out.println("Weight: " + this.weight);
		/// keyword
	}

}
