package oop;

////The public class here will only be used to show the things placed in the file
public class AccessModifers {

	public static void main(String[] args) {

		User myUser = new User();
		myUser.username = "MyUserName"; /// I can access it and change it
//		myUser.password = 123; ///I cannot access it at all

//		myUser.generatePassword(); /// now the password will be generated
//		myUser.showPassword();

		java.util.Scanner sc = new java.util.Scanner(System.in);
		System.out.println("Write the password:");
		int passEntered = Integer.parseInt(sc.nextLine());

		myUser.enterThePassword(passEntered);
//		myUser.checkPassword(passEntered);

		myUser.showPassword();
		sc.close();
	}

}

class User {
///The usually used approach for the private attributes is that -
	//1) The attribute is defined as the private
	///2) But we also have the public methods and we change/read the private attributes using them
	
	
	public String username;
	private int password;

	///// Ex. We restrict the access to the private attributes/methods to not allow
	///// the values to be changed or impacter directly from the other class
	public void showPassword() {
		System.out.println("The password is: " + this.password);
	}

	////In this case, the password must be validated before we assign it to the object's attribute
	public void enterThePassword(int password) {
////Ex. for the password must have have the lenght > 2 digits
		if (password < 100)
			System.out.println("The password is too short!");
		else
			this.password = password;

	}

	public void generatePassword() {
		this.password = (((int) Math.random()) * 10) + 1;
	}

	public void checkPassword(int pass) {
		if (pass == this.password)
			System.out.println("Password is correct");
		else
			System.out.println("Password is not correct");
	}

}
