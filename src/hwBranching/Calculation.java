package hwBranching;

import java.util.Scanner;

public class Calculation {

	public static void main(String[] args) {

		/// Define scanner, to add the option to write from the console
		Scanner myScanner = new Scanner(System.in);

		System.out.println("Enter the first number:");
		double number1 = Double.parseDouble(myScanner.nextLine());

		System.out.println("Enter operator ^,*, /, %, +,-, p, b or s");
		char operator = myScanner.nextLine().charAt(0);

		System.out.println("Enter the second number:");
		double number2 = myScanner.nextDouble();

		double outcome = 2;

		switch (operator) {
		case '*':
			outcome = number1 * number2;
			break;
		case '/':
			outcome = number1 / number2;
			break;
		case '+':
			outcome = number1 + number2;
			break;
		case '-':
			outcome = number1 - number2;
			break;
		case '%':
			outcome = number1 % number2;
			break;
		case 'p':
			System.out.println("Element " + number1 + " and element " + number2);
			// format("%.1f '' %.1f", number1, number2);
			myScanner.close();
			return;
		case 'b':
			System.out.println("Biggest element: "
					+ (number1 == number2 ? " none of them!" : (number1 > number2 ? number1 : number2)));
			//// (number1 > number2 ? number1 : number2));
			////// condition ? action when true : action when false;
//		Math.max(number1, number2));
			myScanner.close();
			return;
		case 's':
			System.out.println("Smaller element: "
					+ (number1 == number2 ? " none of them!" : (number1 < number2 ? number1 : number2)));
			myScanner.close();
			return;
		default:
			System.out.println("You've entered " + operator + ", which is incorrect");
			myScanner.close();
			return;
		}
		System.out.println("The result is " + outcome);
		myScanner.close();
	}

}
