package hwBranching;

import java.util.Scanner;

public class Grade {

	private static void showResult(char grade) {

		switch (grade) {
		case 'A', 'B':
			System.out.println("Perfect! You are so clever!");
			break;
		case 'C':
			System.out.println("Good! But You can do better!");
			break;
		case 'D', 'E':
			System.out.println("It is not good! You should study!");
			break;
		case 'F':
			System.out.println("Fail! You need to repeat the exam!");
			break;
		default:
			System.out.println("Incorrect grade ");
		}
	}

	public static void main(String[] args) {

		Scanner myScanner = new Scanner(System.in);
		System.out.println("Enter your grade: ");
		char grade = myScanner.nextLine().charAt(0);

		Grade.showResult(grade);

		myScanner.close();
	}
}
