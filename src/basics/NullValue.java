package basics;

public class NullValue {

	public static void main(String[] args) {

		int key = 5;
		String var = "";

		if (key == 4)
			var = "3";
		else
			var = "";

//		switch (key) {
//		case 3:
//			var = "3";
//			break;
//		default:
//
//			break;
//		}

		System.out.println(var);

	}

}
