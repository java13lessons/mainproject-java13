package basics;

public class Test {

	public static int x;
	
	public static void main(String[] args) {
		int x;
		
//		Test.x != x;
		
		String str = "Text" + 3;//// Text3
		String str2 = "Text" + "Three";
		System.out.println(str);
		System.out.println(str2);
		System.out.println(3);
		System.out.print("Text1");
		System.out.print("Text2");
		
		
		//////Confusing
		tooMuchParametersPlease(32, "Text", "here", 323.32, "!", 4343.34);
	}

	////Not a good practise! Don't use so much parameters!
	public static void tooMuchParametersPlease(int a, String text, String text2, double val, String text3,
			double val3) {

	}

}
