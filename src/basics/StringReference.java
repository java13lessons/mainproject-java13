package basics;

public class StringReference {

	public static void main(String[] args) {

		String str1 = "Name1"; //// this is the object
		String keep = str1; ///// object located under str1 is passed to the variable keep

		str1 = str1 + "suffix"; /// new object is created

//		System.out.println(keep == str1);

		String string1 = "Name";
		String string2 = "NameSurname";

		string1 += "Surname"; // string1 = string1 + "Surname"'
		////// value of string1 equals NameSurname
		System.out.println(string1 == string2);/// -->false, because == <---- checks the object
		System.out.println(string1.equals(string2));/// -->true, because equals checks the value

	}

}
