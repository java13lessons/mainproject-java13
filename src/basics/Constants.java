package basics;

public class Constants {

	//// We use constants, to place the common values, which are used in differnt
	//// classes/ methods. To not write the same values (which are fixed) multiple
	//// times, but simply use one constant
	final static String LOCAL_HOST_ADDRESS = "127.0.0.1";
	static String var = "abcf";

	/// For all the constants - VARIABLE CAN ONLY BE ASSIGNED ONCE!!!!
	public static void main(String[] args) {
		int var1 = 34343;
		final int CONST1 = 94342;/// variable is initialized and cannot be changed after
		final int CONST2; /// variable is not initialized and can be changed after, but only once!!!

		var1 = 32;/// we can change it
//		CONST1 = 4343;///we can't change it, because it is declared as final

		CONST2 = 23232;
//		CONST2 = 43849832; cannot change it, because the value is already assigned before!

	}

}
