package assgmntOffcer;

import java.util.ArrayList;

public class Execution {

	public static void main(String[] args) {
		Officer[] officers = new Officer[7];
//		District[] districts = new District[2];

		///// we create empty officers, with no information provided about them
		///// to fully test the logic, you can create the officers with the information
		for (int i = 0; i < 7; i++)
			officers[i] = new Officer();

		officers[0].setCrimesSolved(21);
		officers[0].setName("Jack");
		officers[0].setSurname("Black");
		officers[0].setOfficerID(34);
		////// ....... the same can be done for all the officers

		//// We can also define the Officers like this:
		officers[1] = new Officer("Nick", "White", "District 27", 12, 19);
		/// In this case (in the execution class) there is no difference if you store
		/// the elements in the array or in the separete variables

		District district1 = new District("District51", "New-York", 12);
		District district2 = new District("District21", "Washington", 22);

		for (int i = 0; i < 3; i++)
			district1.addNewOfficer(officers[i]);

		for (int i = 3; i < 7; i++)
			district2.addNewOfficer(officers[i]);

		System.out.println(district1);
		System.out.print(System.lineSeparator());
		System.out.println(district2);
		System.out.print(System.lineSeparator());

		district2.removerOfficer(officers[6]);

		System.out.println("Average level in the first district: " + district1.calculateAvgLevellInDistrict());
		System.out.println("Average level in the second district: " + district2.calculateAvgLevellInDistrict());

		System.out.println(
				"There are " + district1.getOfficersInTheDistrict().size() + " officers in the first district");
		System.out.println(
				"There are " + district2.getOfficersInTheDistrict().size() + " officers in the second district");

		ArrayList<Officer> officersInTheFirstDistrict = district1.getOfficersInTheDistrict();
		ArrayList<Officer> officersInTheSecondDistrict = district2.getOfficersInTheDistrict();
		District commonDistrict = new District("District1", "London", 323);
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheFirstDistrict);//// we can add the arraylist to
																						//// the arraylist. It means we
																						//// that we just add new
																						//// elements to the arraylist
		commonDistrict.getOfficersInTheDistrict().addAll(officersInTheSecondDistrict);
		System.out.println(commonDistrict.calculateAvgLevellInDistrict());
		System.out.println("There are " + commonDistrict.getOfficersInTheDistrict().size() + " in Common district");

		/// j.

		if (district1.calculateAvgLevellInDistrict() == district2.calculateAvgLevellInDistrict())
			System.out.println("Levels are the same");
		else
			System.out.println((district1.calculateAvgLevellInDistrict() < district2.calculateAvgLevellInDistrict())
					? "District 2 has better av. level"
					: "District 1 has better level");
	}

}
