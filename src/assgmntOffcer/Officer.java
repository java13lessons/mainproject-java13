package assgmntOffcer;

public class Officer {

	private String name, surname;
	private int officerID, crimesSolved;

	public Officer() {
		// TODO Auto-generated constructor stub
	}

	public Officer(String name, String surname, String workingDistrict, int officerID, int crimesSolved) {
		this.name = name;
		this.surname = surname;
		this.crimesSolved = crimesSolved;
		this.officerID = officerID;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getOfficerID() {
		return officerID;
	}

	public void setOfficerID(int officerID) {
		this.officerID = officerID;
	}

	public int getCrimesSolved() {
		return crimesSolved;
	}

	public void setCrimesSolved(int crimesSolved) {
		this.crimesSolved = crimesSolved;
	}

	//// by default - toString prints the address to the memory (the address of the
	//// reference)
	//// if we override it - it prints what we return from the method
	@Override
	public String toString() {
		return "Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname + System.lineSeparator()
				+ "Officer ID: " + this.officerID + System.lineSeparator() + System.lineSeparator() + "Crimes Solved: "
				+ this.crimesSolved;
	}

	int calculateLevel() {
		if (this.crimesSolved < 20)
			return 1;
		else if (this.crimesSolved >= 20 && this.crimesSolved < 40)
			return 2;
		else
			return 3;

	}

}
