package oopAsgmnt;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Library {
	// Add the missing implementation to this class
	private static String open = "9 AM";
	private static String close = "5 PM";
	private String address;///// instance attribute, because addresses are different for all the libraries
	private List<Book> books = new ArrayList<Book>();///// collection of the books

	public Library(String address) {
		this.address = address;
	}

	public void addBook(Book book) {
		this.books.add(book);/// book is added to the list
	}

	public static void printOpeningHours() {
		System.out.println("From " + open + " to " + close);
	}

	public void printAddress() {
		System.out.println("Address : " + this.address);
	}

	public void borrowBook(String bookname) {
		//// to borrow the book, we need to go through all the books in the library and
		//// find the one to borrow
		Iterator<Book> iterator = this.books.iterator();//// iterator used to go through all books in the collection
		while (iterator.hasNext()) {
			Book book = iterator.next();
			if (book.getTitle().equals(bookname) && !book.isBorrowed()) {
				book.borrowed(); /// if the book is available - borrow it!
				System.out.println("You successfully borrowed " + bookname);
				return; /// since the book if found - finish the method
			}

		}
		/// This point of the method can only be reached if no book is available
		System.out.println("The book is not available");

	}

	public void returnBook(String bookname) {

		Iterator<Book> iterator = this.books.iterator();
		while (iterator.hasNext()) {
			Book book = iterator.next();
			if (book.getTitle().equals(bookname) && book.isBorrowed()) {
				book.returned();
				System.out.println("You successfully returned " + bookname);
				return;
			}

		}
		/// This point of the method can only be reached if there is no book return
		System.out.println("The book is not from this library!");

	}

	public void printAvailableBooks() {
		boolean booksAvailable = false;
		Iterator<Book> iterator = this.books.iterator();
		while (iterator.hasNext()) {
			Book book = iterator.next();
			if (!book.isBorrowed()) {
				System.out.println(book.getTitle());
				booksAvailable = true;
			}
		}
		if (!booksAvailable) {
			System.out.println("No book in catalog");
		}
	}

	public static void main(String[] args) {
		// Create two libraries
		Library firstLibrary = new Library("10 Main St.");
		Library secondLibrary = new Library("228 Liberty St.");

		// Add four books to the first library
		firstLibrary.addBook(new Book("The Da Vinci Code"));
		firstLibrary.addBook(new Book("Le Petit Prince"));
		firstLibrary.addBook(new Book("A Tale of Two Cities"));
		firstLibrary.addBook(new Book("The Lord of the Rings"));

		// Print opening hours and the addresses
		System.out.println("Library hours:");
		printOpeningHours();
		System.out.println();

		System.out.println("Library addresses:");
		firstLibrary.printAddress();
		secondLibrary.printAddress();
		System.out.println();

		// Try to borrow The Lords of the Rings from both libraries
		System.out.println("Borrowing The Lord of the Rings:");
		firstLibrary.borrowBook("The Lord of the Rings");
		firstLibrary.borrowBook("The Lord of the Rings");
		secondLibrary.borrowBook("The Lord of the Rings");
		System.out.println();

		// Print the titles of all available books from both libraries
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
		System.out.println();
		System.out.println("Books available in the second library:");
		secondLibrary.printAvailableBooks();
		System.out.println();

		// Return The Lords of the Rings to the first library
		System.out.println("Returning The Lord of the Rings:");
		firstLibrary.returnBook("The Lord of the Rings");
		System.out.println();

		// Print the titles of available from the first library
		System.out.println("Books available in the first library:");
		firstLibrary.printAvailableBooks();
	}
}