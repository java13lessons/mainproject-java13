package scanner;

//import java.util.Scanner;

public class TestScanner {

	public static void main(String[] args) {
		java.util.Scanner sc = new java.util.Scanner(System.in);///// create scanner object and pass to sc variable
		System.out.println("Input one integer below:");

		Integer val = Integer.parseInt(sc.nextLine());//// methodcall sc.nextLine() returns the String
		///// Integer class can also be used as the type for the integer values
		System.out.println("Thanks! Your input was: " + val + "!");

		// System.out.println("Input one character below:");
//
//		char val = sc.nextLine().charAt(0);//// methodcall sc.nextLine() returns the String
//		///// charAt(0) method return the first character of the string
//		System.out.println("Thanks! Your input was: " + val + "!");

		// System.out.println("Input integer number below:");
//
//		int val = Integer.parseInt(sc.nextLine());////methodcall sc.nextLine() returns the String 
//		/////value, which is passed to the method Integer.parseInt ====> we get integer value
//		System.out.println("Thanks! Your input was: " + val + "!");

//		System.out.println("Input text below:");
//
//		String text = sc.nextLine();
//		System.out.println("Thanks! Your input was: " + text + "!");

		sc.close();
	}

}
