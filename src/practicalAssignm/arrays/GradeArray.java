package practicalAssignm.arrays;

public class GradeArray {

	public static void main(String[] args) {
		byte[] arr = new byte[10];
		java.util.Scanner sc = new java.util.Scanner(System.in);

		for (int i = 0; i < 10; i++) {
			byte inputValue = Byte.parseByte(sc.nextLine());

			if (inputValue >= 0 && inputValue <= 10)
				arr[i] = inputValue;
		}

		int numberOfFailed = 0;

		for (int grade : arr) {
			if (grade < 4)
				numberOfFailed++;
		}

		System.out.println(numberOfFailed + " students  failed the test");

		int numberOfA = 0;

		for (int grade : arr) {
			if (grade == 10)
				numberOfA++;
		}

		System.out.println(numberOfA + " students  got 10");

		///// after the initialization grades == [0,0,....,0]
		byte[] grades = new byte[11];/// the array to store the number of students
		///// who got the corresponding mark
		///// 2 students got 0 ==> grades[0] is 2; 5 students got 9 ==> grades[9] is
		///// 5...

		for (int grade : arr)
			grades[grade]++; ////since the grade represents the key/index of the array, we can use it as the
		                    /////index to access the corresponding elemetn of the array

		for (int i = 0; i < grades.length; i++)
			System.out.println(grades[i] + " students with the grade " + i);

		sc.close();

	}

}
