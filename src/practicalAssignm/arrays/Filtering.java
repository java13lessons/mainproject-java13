package practicalAssignm.arrays;

public class Filtering {

	public static void main(String[] args) {
		int[] elements = { 10, 4, -4, 7, 0, 9, 1, 3, 0, 7, -5, 4 };

		for (int element : elements) {
			if (element < 0) {
				System.out.print(element);
				System.out.print(' ');
			}
		}
		System.out.print(System.lineSeparator());

		for (int element : elements) {
			if (Math.abs(element) % 2 == 1) {
				System.out.print(element);
				System.out.print(';');
			}
		}
		System.out.print(System.lineSeparator());

		int counter = 0;
		for (int element : elements) {
			if (Math.abs(element) % 2 == 1)
				counter++;
		}

		System.out.println("There are " + counter + " odd elements in the array");

		boolean sameElements = false;

//		System.out.print("The same elements are ");

		for (int i = 0; i < elements.length - 1; i++) {
			for (int j = i + 1; j < elements.length; j++) {
				if (elements[i] == elements[j]) {
					if (sameElements == false)
						System.out.print("The same elements are ");

					sameElements = true;
					System.out.print(elements[i]);
					System.out.print(" ");
				}

			}
		}

		System.out.print(System.lineSeparator());
		if (!sameElements)
			System.out.println("No same elements in the array");

//		if (sameElements)
//			System.out.println("There are the same elements in the array");
//		else
//			System.out.println("No same elements in the array");

		System.out.print(System.lineSeparator());
		for (int i = 0; i < elements.length; i += 2) {
			System.out.print(elements[i]);
			System.out.print(' ');
		}
		System.out.print(System.lineSeparator());

		double sum = 0;

		for (int element : elements)
			sum += element;

		double averageVal = sum / elements.length;
		System.out.println("The average value is " + averageVal);

		int smallerElements = 0;
		for (int element : elements) {
			if (element < averageVal)
				smallerElements++;
		}

		System.out.println("There are " + smallerElements + 
				" elements which are smaller than the average value");

	}

}
