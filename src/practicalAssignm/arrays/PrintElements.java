package practicalAssignm.arrays;

public class PrintElements {

	public static void main(String[] args) {
		int[] arr = { 5, 3, 7, 6, 2, 8 };

		for (int i = 0; i < arr.length; i++) {
//			System.out.println(arr[i]);////print the number, each number on different line
			///// Print at the same line
			System.out.print(arr[i]);/// print the element
			System.out.print(";");///// prints the semicolon
		}
		System.out.print(System.lineSeparator());

		for (int val : arr) {
			System.out.print(val);/// print the element
			System.out.print(" ");///// prints the space
		}
		System.out.print(System.lineSeparator());

		int i = 0;

		while (i < arr.length) {
			System.out.print(arr[i]);/// print the element
			System.out.print("_");///// prints the underscore
			i++;
		}
		System.out.print(System.lineSeparator());

		i = 0;
		do {
			System.out.print(arr[i]);/// print the element
			System.out.print(":");///// prints the colon
			i++;
		} while (i < arr.length);

	}

}
