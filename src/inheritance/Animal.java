package inheritance;

////We can't write - new Animal();  (cannot create the instance of Animal class itself, because this is abstract class)
/// Only the objects from the subclasses of Animal can be created
public abstract class Animal {

	private int age;

	////// ALL THE ABSTRACT METHODS MUST BE IMPLEMEMNTED (override and write the
	////// logic) IN THE SUB-CLASS WHICH EXTENDS THE ABSTRACT CLASS
	abstract public void move();
//	public void move() {
//		
//	}
	///// If we change the definition of the abstract method to non-abstract method
	///// - this will not affect the logic at all

	//// there can also be non abstract methods
	public void eat() {
		System.out.println("I'm eating");
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getAge() {
		return age;
	}

	///// private methods can also be addded in the abstract class

}
