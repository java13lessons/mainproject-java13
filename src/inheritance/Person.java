package inheritance;

///We want to use all these properties in the classes: CEO and Employee (because they both are the persons)
//final public class Person {//// if the class is declared as final - IT CAN'T BE EXTANDED! 
public class Person {//// One class can have multiple sub-classes
	/// If we use protected access modifer - the variable will be available for the
	/// sub-classes of the class Person

	//// all the other properties are mostly the same as for the private attributes
	protected String name, surname;
	protected int age;
	private boolean stomachFull = false;
//	public int yearsAtPostion;
//	private int age;///cannot use it as private, because it won't be avalaible in the sublasses then

	private Person dad, mother;

	public Person(String name, String surname, int age) {
		this.name = name;
		this.surname = surname;
		this.age = age;
	}

	public void describe() {
		System.out.println("Name: " + this.name + System.lineSeparator() + "Surname: " + this.surname
				+ System.lineSeparator() + "Age: " + this.age);
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	public void eat() {
		this.stomachFull = true;
	}

}
