package inheritance;

/////All the methods defined in the interfaces must be default/public
public interface RidableAnimal {

	/// In the interfaces we mostly define just the signatures of the methods
	/// (signature of the method - returning type/void, name of the method, all the
	/// parameters)
	public void ride();

	void setRaces(int numberOfRaces);

	int getRaces();

}
