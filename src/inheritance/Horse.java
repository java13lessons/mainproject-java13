package inheritance;

////Animal class (superclass) is called MORE GENERAL, and the class Horse (sub-class) is called MORE SPECIFIC
///// We also say that RidableAnimal interface is MORE GENERAL
public class Horse extends Animal implements RidableAnimal {

	private int races;

	@Override
	public int getRaces() {
		return this.races;
	}

	@Override
	public void setRaces(int numberOfRaces) {
		this.races = numberOfRaces;
	}

	@Override
	public void move() {
		this.ride();
	}

	@Override
	public void ride() {
		System.out.println("Horse is riding");
	}

}
