package inheritance;

import java.util.ArrayList;

public class InvestmentAccount extends BankAccount {

	private ArrayList<String> portfolio = new ArrayList<String>();////// portfolio of the stock we have in the account

	public ArrayList<String> getPortfolio() {
		return portfolio;
	}

	public void buyStock(String stockName) {
		this.portfolio.add(stockName);
	}

	public void sellStock(String stockName) {
		if (this.portfolio.contains(stockName))// check if the stock exists in the portfolio
			this.portfolio.remove(stockName);
		else
			System.out.println("The stock " + stockName + " does not exist in the portfolio!");
	}

}
