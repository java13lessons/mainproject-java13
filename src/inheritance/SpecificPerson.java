package inheritance;

public class SpecificPerson extends Person implements Player, Parent {

	private String game;
	private Person kid;

	public void setKid(Person kid) {
		this.kid = kid;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public SpecificPerson(String name, String surname, int age) {
		super(name, surname, age);
	}

	@Override
	public void play() {
		System.out.println(this.name + this.surname + " plays " + this.game);
	}

	@Override
	public void feedKid() {
		this.kid.eat();
	}

}
