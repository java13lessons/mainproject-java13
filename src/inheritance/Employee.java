package inheritance;
///The main idea of the inheritance is - in the subclasses we want to add something more (extend the superclass)

///We define that Employee is also the Person
public class Employee extends Person {/// each class can have ONLY superclass

	//// public and protected from the class Person will be available in this class
	private int claficationLevel;

	/// 1.The object Employee class cannot exist (cannot be compiled), if the
	/// constructor is not defined, because we only parametrized contructor in the
	/// superclass
	/// 2 The constructor from the superclass must be called
	public Employee(String name, String surname, int age, int level) {
		super(name, surname, age);/// this is how we call/access the constructor in the superclass
		this.claficationLevel = level;/// a small extension of the contructor
	}

	public int getSalary() {
		/// age variable comes from Person class
		return this.age * this.claficationLevel;
//				* this.yearsAtPostion;//// we can see that age attribute is also the
		//// part of the current
		//// object
	}

	///// this (keyword) - it is the object!
	//// super - it is NOT the object

	/// What we need to do, if the method describe in the class Employee should be
	/// different from the method in the class Person?
	@Override /// Override comes, when override (rewrite the method from the superclass)
	public void describe() {

		super.describe();/// using super. we can access the implementation of the methods in the
							/// superclass
		System.out.println("The clafication level: " + this.claficationLevel);
//		System.out.println("This method is overriden");
	}

	public static void main(String[] args) {
		Employee employee = new Employee("Donald", "Trump", 70, 3);
		employee.describe();/// the method from the superclass - Person
	}

}
