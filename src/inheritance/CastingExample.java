package inheritance;

public class CastingExample {

	public static void main(String[] args) {
		Dog dogObj = new Dog();//// create the instacne of Dog
		Horse horsObj = new Horse(); /// instance of Horse is created
		Animal animVar;
		RidableAnimal ridAnimal;

		/// The casting below is done automatically, because Dog and Horse are the
		/// sub-classes of the class Animal
		animVar = dogObj;/// cast Dog to Animal

		/// we call the methods from Animal class
//		animVar.move();
//		animVar.eat();

		/// at this point - the reference of Dog is located under the variable animVar
//		ridAnimal = (RidableAnimal) animVar; /// we will have ClassCastException, because we try to pass the reference
		/// of Dog to RidableAnimal, but Dog class does not implememnt
		/// RidableAnimal interface

		////// Syntax: the variable instanceof the type. Check if the object (located
		////// under the variable) has the type specifed on the right side
		if (animVar instanceof RidableAnimal)/// returns false
			ridAnimal = (RidableAnimal) animVar;///// this line won't be triggered, because animVar instanceof
												///// RidableAnimal returns false
		System.out.println(animVar instanceof RidableAnimal);
		
		animVar = horsObj;/// cast Horse to Animal
//		animVar.move();
//		animVar.eat();

		ridAnimal = horsObj;//// cast Horse to Ridebale object
//		ridAnimal.ride(); ///we can call the method which is defined in the interface

		/// At this point of code - the reference of Horse is assigned to the variable
		/// animVar

		if (animVar instanceof RidableAnimal)/// returns true
			ridAnimal = (RidableAnimal) animVar;///// dowcasting from animVar to ridAdniaml

		System.out.println(animVar instanceof RidableAnimal);

		//// no errors at this point, because the reference of Horse is located under
		//// the variable animVar
//		ridAnimal.ride();

		Object obj;
		obj = ridAnimal;  ////all the types of object can be casted to Object variable
		obj = animVar;
		
		
	}

}
