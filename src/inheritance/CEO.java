package inheritance;

//////It is also possible to extend Employee (or any another)
//// If we extend Empoyee class here it will look that Person is the superclass of Employee 
/////and the Employee is the superclass of CEO. WE GO FROM MORE GENERAL TO MORE DETAILED (that's the key for the concept of the inheritance)
public class CEO extends Person {

	private int yearsAtThePosition;

	public CEO(String name, String surname, int age, int years) {
		super(name, surname, age);/// this is how we call/access the constructor in the superclass
		this.yearsAtThePosition = years;
	}

	public int getDividends() {
		return this.age * 1000;
	}

	@Override
	public void describe() {
		// TODO Auto-generated method stub
		super.describe();
		System.out.println("Years at the position: " + this.yearsAtThePosition);
	}

	public static void main(String[] args) {
		CEO ceo = new CEO("Bill", "Gates", 60, 30);
		ceo.describe();/// The class Employee and CEO extend the class Person differently
	}

}
