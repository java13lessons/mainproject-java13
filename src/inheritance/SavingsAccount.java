package inheritance;

public class SavingsAccount extends BankAccount {

	private int term;
	private int termPassed;
	private int percent;

	public void setPercent(int percent) {
		this.percent = percent;
	}

	public void setTerm(int term) {
		this.term = term;
	}

	public int getPercent() {
		return percent;
	}

	public int getTerm() {
		return term;
	}

	public void setTermPassed(int termPassed) {
		this.termPassed = termPassed;
	}

	public int getTermPassed() {
		return termPassed;
	}

	@Override
	public void withdrawMoney(int amount) {
		if (this.term > this.termPassed)
			super.withdrawMoney(amount);
		else
			System.out.println("You can't withdraw the money now!");
	}

}
