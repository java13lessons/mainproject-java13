package inheritance;

public class Dog extends Animal implements Pet {

	private String name;

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return this.name;
	}

	@Override
	public void setName(String name) {
		this.name = name;
	}

	@Override
	public void play() {
		System.out.println("Dog is playing");
	}

	@Override
	public void move() {
		System.out.println("Dog is walking");
	}

}
