package inheritance;

public interface Player {
//	public interface Player extends Parent{ it is possible that the interface extends another interface

//	int attr;//// can't define the attributes in the interface
//	final int constantValue = 3232; - we can define the constant value

	/// no difference between declaring as public or default
	public void play();//// we can hav the methods

//	protected void method(); cannot add protected methods

}
