package inheritance;

public class BankAccount {

	protected int amountOfCash;

	public int getAmountOfCash() {
		return amountOfCash;
	}

	public void setAmountOfCash(int amountOfCash) {
		this.amountOfCash = amountOfCash;
	}

	public void withdrawMoney(int amount) {
		if (this.amountOfCash >= amount)
			this.amountOfCash -= amount;
		else
			System.out.println("Not enough money!");
	}

	public void deposit(int amount) {
		this.amountOfCash += amount;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
