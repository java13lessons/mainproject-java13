package filesInJava;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class FileReadExample {

	public static void main(String[] args) throws IOException {
		String path = "C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\100h\\WorkspaceJava13\\MainProject\\src\\filesInJava\\FileReadExample.java";
		File fileCSV = new File(path);
		BufferedReader reader = new BufferedReader(new FileReader(fileCSV, StandardCharsets.UTF_8));
		StringBuffer strBuild = new StringBuffer();
		/// the logic to read the content
		String line = reader.readLine(); // read the first line
		while (line != null) { /// check if we haven't reached the end file (null - no more lines)
			strBuild.append(line); /// append the line to out "final" result
			strBuild.append(System.lineSeparator());/// also need to add the line separator, to go to the next line
			line = reader.readLine();/// read next line
		}
		System.out.println(strBuild.toString());
		reader.close();
	}

}
