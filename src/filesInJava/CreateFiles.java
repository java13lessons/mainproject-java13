package filesInJava;

import java.io.FileWriter;
import java.io.IOException;

public class CreateFiles {

	public static void main(String[] args) throws IOException {
		FileWriter writer = new FileWriter("C:\\Users\\Arturs Olekss\\Documents\\JAVA_lessons\\BuildedText.txt");
		java.util.Scanner sc = new java.util.Scanner(System.in);
		boolean finish = false;
		StringBuffer strBuf = new StringBuffer();/// we will build the string (text file)
		while (!finish) {
			String line = sc.nextLine();
			if (line.equals("f"))/// if we type "f", it means that we want to finish
				finish = true;
			else {
				strBuf.append(line);/// here we build the content of the file
				strBuf.append(System.lineSeparator());
			}
		}

		writer.write(strBuf.toString());
		writer.close();
		sc.close();

	}

}
