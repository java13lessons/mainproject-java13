package overloading;

import java.util.ArrayList;

public class Example {

	public static void main(String[] args) {

		Salary salary = new Salary();
		System.out.println(salary.calculateSalary());/// we search for the method, with the signature calculateSalary()
		System.out.println(salary.calculateSalary(5));/// we search for the method, with the signature
														/// calculateSalary(int)
		System.out.println(salary.calculateSalary("Richard"));/// different method is called
		////// we search for the method, with the signature calculateSalary(String)
		System.out.println(salary.calculateSalary(5, 1000));/// we search for the method, with the signature
															/// calculateSalary(int,int)
	}

}

class Salary {

	final int BASE_SALARY = 1500;

	public double calculateSalary() {
		return BASE_SALARY;
	}

//	public int calculateSalary(int bonus) {/// this cannot be done, because the method with the signature (int
//		/// ...) already exists
//		return BASE_SALARY + bonus;
//	}

	public double calculateSalary(double addPercantage, double bonus) {/// this cannot be done, because the method with
																		/// the
		/// signature (int
		/// ...) already exists
		return BASE_SALARY * (1 + (addPercantage / 100)) + bonus;
	}

	public double calculateSalary(double addPercantage) {
		return BASE_SALARY * (1 + (addPercantage / 100));
	}

	public double calculateSalary(String name) {
		if (name.startsWith("R"))
			return BASE_SALARY - 100;
		else
			return this.calculateSalary();
	}

	/// signature is calculateSalary(String,int)
	public double calculateSalary(String name, int age) {
		if (name.startsWith("R") && age > 20)
			return BASE_SALARY + 100;
		else
			return this.calculateSalary();
	}

	/// change the sequence of the parameters
	/// signature is calculateSalary(int,String). signature is
	/// calculateSalary(int,String) IS NOT THE SAME AS signature is
	/// calculateSalary(String,int)
	public double calculateSalary(int age, String name) {/// if there is different sequence of the parameters provided,
															/// than the method will be considered as different
		if (name.startsWith("R") && age < 20)
			return BASE_SALARY - 100;
		else
			return this.calculateSalary();
	}

}

class Family {

	ArrayList<Child> childrens = new ArrayList<Child>();

	///// we have two methods with the same name, but with different parameters,
	///// this is called overloading
	public void addChild(Child child) {
		this.childrens.add(child);
	}

	public void addChild(Child child1, Child child2) {
		this.childrens.add(child1);
		this.childrens.add(child2);
	}

}

class Child {

}
