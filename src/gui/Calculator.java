package gui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.WindowConstants;

public class Calculator implements ActionListener {

	private JFrame frame = new JFrame("Calculator");
	private JPanel panel = new JPanel();
	private JButton sum = new JButton("+");
	private JButton subtr = new JButton("-");
	private JButton multiply = new JButton("*");
	private JButton division = new JButton("/");
	private JTextField num1 = new JTextField();
	private JTextField num2 = new JTextField();
	private JLabel result = new JLabel();

	public static void main(String[] args) {

		Calculator calculator = new Calculator();

	}

	@Override
	public void actionPerformed(ActionEvent e) {

		int result;

		try {
			/// Check which button is pressed
			if (e.getSource() == this.sum)
				result = Integer.parseInt(this.num1.getText()) + Integer.parseInt(this.num2.getText());
			else if (e.getSource() == this.subtr)
				result = Integer.parseInt(this.num1.getText()) - Integer.parseInt(this.num2.getText());
			else if (e.getSource() == this.multiply)
				result = Integer.parseInt(this.num1.getText()) * Integer.parseInt(this.num2.getText());
			else
				result = Integer.parseInt(this.num1.getText()) / Integer.parseInt(this.num2.getText());

			this.result.setText(String.valueOf(result));
		} catch (Exception exception) {
			this.result.setText("ERROR");
		}
	}

	public Calculator() {
		this.frame.setSize(400, 400);
		this.frame.add(this.panel);
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		GridBagLayout layout = new GridBagLayout();
		this.panel.setLayout(layout);

		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;

		/// Adding the labels
		this.panel.add(new JLabel("Number 1"), constraints);
		constraints.gridx = 1;
		this.panel.add(new JLabel("Number 2"), constraints);
		constraints.gridx = 2;
		this.panel.add(new JLabel("Result"), constraints);

		/// Adding input fields
		constraints.gridx = 0;
		constraints.gridy = 1;
		this.panel.add(this.num1, constraints);

		constraints.gridx = 1;
		this.panel.add(this.num2, constraints);

		constraints.gridx = 0;
		constraints.gridy = 2;
		this.panel.add(this.multiply, constraints);
		this.multiply.addActionListener(this);

		constraints.gridy = 3;
		this.panel.add(this.division, constraints);
		this.division.addActionListener(this);

		constraints.gridy = 4;
		this.panel.add(this.sum, constraints);
		this.sum.addActionListener(this);

		constraints.gridy = 5;
		this.panel.add(this.subtr, constraints);
		this.subtr.addActionListener(this);

		/// Add the result field / label

		constraints.gridx = 2;
		constraints.gridy = 1;
		this.panel.add(this.result, constraints);

		this.frame.setVisible(true);
	}

}
