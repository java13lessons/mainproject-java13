package gui;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

public class Example {

	public static void main(String[] args) {
		MyFrame frame = new MyFrame();
	}

}

class MyFrame implements ActionListener {

	private JFrame frame = new JFrame("Name of the frame");//// main frame
	private JPanel panel = new JPanel();// main, placed under the frame
	private JButton button1 = new JButton("Button1");/// button
	private JButton button2 = new JButton("Button2");/// button
	private JLabel label = new JLabel("Default Label");// label

	///// The method which is called on the click, when the current object (this) is
	///// used as the action listener
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.button1) {
			this.label.setText("Button1 is pressed");
		} else if (e.getSource() == this.button2) {
			this.label.setText("Button2 is pressed");
		}

	}

	public MyFrame() {
		frame.setSize(400, 400);
		this.frame.setVisible(true);
		frame.add(panel);
		GridBagLayout layout = new GridBagLayout();
		GridBagConstraints constraints = new GridBagConstraints();

		this.panel.setLayout(layout);

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridx = 0;
		constraints.gridy = 0;
		panel.add(this.button1, constraints);
		this.button1.addActionListener(this);///// we define, which object will be used as the listener, when the user
												///// clicks on the button

		constraints.gridx = 1;
		constraints.gridy = 0;
		panel.add(this.button2, constraints);/// add the elements to the panel
		this.button2.addActionListener(this);

		constraints.gridx = 0;
		constraints.gridy = 1;
		constraints.gridwidth = 2;//// width = 2 elements;

		panel.add(this.label, constraints);

	}

}