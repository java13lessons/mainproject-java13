package gui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

public class Voting {

	public static void main(String[] args) {
		FillCandidates fillCandidates = new FillCandidates();

	}

}

class Candidate {
	private String name, surname;
	private ImageIcon image;

	public void setImage(ImageIcon image) {
		this.image = image;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public ImageIcon getImage() {
		return image;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	@Override
	public String toString() {
		return this.name + " " + this.surname;
	}

}

class FillCandidates implements ActionListener {

	private JFrame frame = new JFrame("Candidates enter");
	private JPanel panel = new JPanel();
	private JLabel nameLab = new JLabel("Name"), surnameLab = new JLabel("Surname"), pictureLab = new JLabel("Picture"),
			picture = new JLabel();
	private JTextField name = new JTextField(), surname = new JTextField();
	private JButton pictureSelect = new JButton();
	private JButton addCandidate = new JButton("Add candidate");
	private JButton clear = new JButton("Clear");
	private JButton goToVote = new JButton("Go to vote");

	private List<Candidate> candidates = new ArrayList<Candidate>();

	private void clearValues() {
		this.name.setText("");
		this.surname.setText("");
		this.picture.setIcon(null);
		this.pictureSelect.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.pictureSelect) {
			File file = this.fileSelector();

			// Do nothing if the file is not selected
			if (file == null)
				return;

			try {
				BufferedImage picture = ImageIO.read(file);
				ImageIcon image = new ImageIcon(picture.getScaledInstance(90, 90, BufferedImage.TYPE_INT_RGB));
				this.picture.setIcon(image);
				this.picture.setPreferredSize(new Dimension(90, 90));
				this.pictureSelect.setVisible(false);
			} catch (Exception exception) {
				exception.printStackTrace();
			}
		} else if (e.getSource() == this.addCandidate) {
			Candidate candidate = new Candidate();
			candidate.setName(this.name.getText());
			candidate.setSurname(this.surname.getText());
			candidate.setImage((ImageIcon) this.picture.getIcon());
			this.candidates.add(candidate);
			this.clearValues();
		} else if (e.getSource() == this.clear) {
			this.clearValues();
		} else if (e.getSource() == this.goToVote) {
			this.goToVote();
		}

	}

	private File fileSelector() {

		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setDialogTitle("Select the file");

		if (fileChooser.showOpenDialog(this.frame) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			return file;
		}

		return null;
	}

	public FillCandidates() {
		this.frame.setSize(new Dimension(400, 400));
		this.frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		GridBagLayout layout = new GridBagLayout();
		this.panel.setLayout(layout);
		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridy = 0;

		constraints.gridx = 0;
		this.panel.add(this.nameLab, constraints);

		constraints.gridx = 1;
		this.panel.add(this.surnameLab, constraints);

		constraints.gridx = 2;
		this.panel.add(this.pictureLab, constraints);

		constraints.gridy = 1;

		constraints.gridx = 0;
		this.panel.add(this.name, constraints);
		this.name.setPreferredSize(new Dimension(90, 20));

		constraints.gridx = 1;
		this.panel.add(this.surname, constraints);
		this.surname.setPreferredSize(new Dimension(90, 20));

		constraints.gridx = 2;
		this.panel.add(this.pictureSelect, constraints);
		this.pictureSelect.addActionListener(this);
		this.panel.add(this.picture, constraints);

		constraints.gridx = 3;
		this.panel.add(this.addCandidate, constraints);
		this.addCandidate.addActionListener(this);

		constraints.gridy = 2;
		this.panel.add(this.clear, constraints);
		this.clear.addActionListener(this);

		constraints.gridy = 3;
		this.panel.add(this.goToVote, constraints);
		this.goToVote.addActionListener(this);

		this.frame.add(this.panel);
		this.frame.setVisible(true);
	}

	private void goToVote() {
		Candidate[] candidatesArray = new Candidate[this.candidates.size()];
		for (int i = 0; i < candidatesArray.length; i++)
			candidatesArray[i] = this.candidates.get(i);

		VotingFrame votingFrame = new VotingFrame(candidatesArray);

	}

}

class CandidateResults {
	private Candidate candidate;
	private int result;

	public Candidate getCandidate() {
		return candidate;
	}

	public int getResult() {
		return result;
	}

	public void setCandidate(Candidate candidate) {
		this.candidate = candidate;
	}

	public void setResult(int result) {
		this.result = result;
	}

	@Override
	public String toString() {
		return this.candidate.getName() + " " + this.candidate.getSurname() + "; Votes for: " + this.result;
	}

}

class VotingFrame implements MouseListener, ActionListener {
	private JFrame votingFrame = new JFrame("Vote");
	private JList<Candidate> candidates = new JList<Candidate>();
	private JPanel panel = new JPanel();
	private JLabel namesLabel = new JLabel("Names to select");
	private JLabel picture = new JLabel();
	private JButton voteButton = new JButton("Vote");
	private JList<CandidateResults> results = new JList<CandidateResults>();
	private Map<Candidate, CandidateResults> votes = new HashMap<Candidate, CandidateResults>();

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == this.voteButton) {
			Candidate candidateSelected = this.candidates.getSelectedValue();

			if (candidateSelected == null)
				return;

			CandidateResults canidateResult = this.votes.get(candidateSelected);
			int votes = canidateResult.getResult();
			votes++;
			canidateResult.setResult(votes);
			this.results.updateUI();

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		Candidate candidateClicked = this.candidates.getSelectedValue();
		this.picture.setIcon(candidateClicked.getImage());
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	private void initializeVotes(Candidate[] candidates) {
		CandidateResults[] resultsArray = new CandidateResults[candidates.length];
		for (int i = 0; i < candidates.length; i++) {
			CandidateResults candidateResult = new CandidateResults();
			candidateResult.setResult(0);
			candidateResult.setCandidate(candidates[i]);
			this.votes.put(candidates[i], candidateResult);
			resultsArray[i] = candidateResult;
		}
		this.results.setListData(resultsArray);
	}

	public VotingFrame(Candidate[] candidates) {
		this.votingFrame.setSize(new Dimension(400, 400));
		this.votingFrame.add(this.panel);
		this.initializeVotes(candidates);

		GridBagLayout layout = new GridBagLayout();
		this.panel.setLayout(layout);

		GridBagConstraints constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.HORIZONTAL;
		constraints.gridy = 0;
		constraints.gridx = 0;
		this.panel.add(this.namesLabel, constraints);

		constraints.gridy = 1;
		this.candidates.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
		this.candidates.setLayoutOrientation(JList.HORIZONTAL_WRAP);
		this.panel.add(this.candidates, constraints);
		this.candidates.addMouseListener(this);

		this.candidates.setListData(candidates);

		constraints.gridx = 1;
		this.panel.add(this.picture, constraints);

		constraints.gridy = 1;
		this.panel.add(this.voteButton, constraints);
		this.voteButton.addActionListener(this);

		constraints.gridx = 1;
		constraints.gridy = 2;
		this.panel.add(this.results, constraints);
		this.results.enableInputMethods(false);

		this.votingFrame.setVisible(true);
		this.votingFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

	}
}
